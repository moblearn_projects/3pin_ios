//
//  UserRepository.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 10/4/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

struct UserRepository {
    
    var ref : DatabaseReference;
    
    init() {
        self.ref = Database.database().reference().child("users");
    }
    
    func loginWithFacebook(_ token: String, completion: @escaping (User?, Error?) -> Void ) -> Void {
        
        let credential = FacebookAuthProvider.credential(withAccessToken: token)
        print("CEDENTIAL: \(credential.provider)")
        
        Auth.auth().signIn(with: credential) { (user, error) in
    
            guard user != nil else {
                completion(nil, HandleError.handleFIRAuthErrorCode(error!))
                return
            }
            
//            if let error = error {
//                let loginError = HandleError.handleFIRAuthErrorCode(error)
//                completion(nil, loginError)
//                return
//            }
            
            completion(user, nil);
        }
    }
    
    func getFriendsCollection(_ userID:String, completion : @escaping ([Friends]?, Error?) -> Void) -> Void {
        ref.child(userID).child("friends").observeSingleEvent(of: .value, with: { (snapshot) in
            var itens = [Friends]()
            
            guard snapshot.childrenCount > 0 else {
                completion(itens, nil)
                return
            }
            
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                if let value = item.value as? NSDictionary {
                    let friend = Friends(value: value as! [String:NSObject])
                    if friend.accept! {
                        itens.append(friend)
                    }
                }
            }
            completion(itens, nil)
        }) {(error) in
            completion(nil, error)
        }
    }
    
    func getFriends(_ userID:String, completion : @escaping ([Friends]?, Error?) -> Void) -> Void {
        ref.child(userID).child("friends").observeSingleEvent(of: .value, with: { (snapshot) in
            var itens = [Friends]()
            
            guard snapshot.childrenCount > 0 else {
                completion(itens, nil)
                return
            }
            
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                if let value = item.value as? NSDictionary {
                    let friend = Friends(value: value as! [String:NSObject])
                    itens.append(friend)
                }
            }
            completion(itens, nil)
        }) {(error) in
            completion(nil, error)
        }
    }
    
    func removePeople(uid:String, idFriend:String, completion : @escaping (Bool?, Error?) -> Void) -> Void {
        ref.child(uid).child("friends").child(idFriend).removeValue { (error, reference) in
            guard error == nil else {
                completion(false, error?.localizedDescription as? Error)
                return
            }
            self.ref.child(idFriend).child("friends").child(uid).removeValue()
            self.deleteFriend(idFriend, completion: { (sucess, error) in
                guard error == nil else {
                    completion(false, error?.localizedDescription as? Error)
                    return
                }
                completion(true, nil)
            })
        }
    }
    
    func getFriendsPendent(_ userID:String, completion : @escaping ([Friends]?, Error?) -> Void) -> Void {
        ref.child(userID).child("friends").observeSingleEvent(of: .value, with: { (snapshot) in
            var itens = [Friends]()
            
            guard snapshot.childrenCount > 0 else {
                completion(itens, nil)
                return
            }
            
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                if let value = item.value as? NSDictionary {
                    let friend = Friends(value: value as! [String:NSObject])
                    if !friend.acceptMe! {
                        itens.append(friend)
                    }
                }
            }
            completion(itens, nil)
        }) {(error) in
            completion(nil, error)
        }
    }
    
    func countFriendsPendent(_ userID:String) -> Int {
        
        var total:Int = 0
        
        ref.child(userID).child("friends").observeSingleEvent(of: .value, with: { (snapshot) in
            var itens = [Friends]()
            
            guard snapshot.childrenCount > 0 else {
                return
            }
            
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                if let value = item.value as? NSDictionary {
                    let friend = Friends(value: value as! [String:NSObject])
                    if !friend.acceptMe! {
                        itens.append(friend)
                    }
                }
            }
            total = itens.count
        })
        return total
    }
    
    func acceptFriendMe(_ friend:Friends, completion : @escaping (Bool?, Error?) -> Void) -> Void {
        
        let uid = self.getUid()
        let objFriend = Friends.init(value: ["accept":true as AnyObject,
                                             "acceptMe":true as AnyObject,
                                             "nome":friend.name as AnyObject,
                                             "photo":friend.photo as AnyObject,
                                             "uid":friend.uid as AnyObject])
        
        self.ref.child(uid).child("friends").child(friend.uid!).setValue(objFriend.toDictionary()) { (error, reference) in
            
            guard (error == nil) else {
                completion(false, error)
                return;
            }
            completion(true, nil)
        }
        
    }
    
    func acceptMeFriend(_ friend:Friends, objUser:UserModel, completion : @escaping (Bool?, Error?) -> Void) -> Void {
        
        let uid = self.getUid()
        let objFriend = Friends.init(value: ["accept":true as AnyObject,
                                             "acceptMe":true as AnyObject,
                                             "nome":objUser.name as AnyObject,
                                             "photo":objUser.photo as AnyObject,
                                             "uid":objUser.uid as AnyObject])
        
        self.ref.child(friend.uid!).child("friends").child(uid).setValue(objFriend.toDictionary()) { (error, reference) in
            
            guard (error == nil) else {
                completion(false, error)
                return;
            }
            completion(true, nil)
        }
        
    }
    
    func addFriend(_ friend:Friends, completion : @escaping (Bool?, Error?) -> Void) -> Void {
        
        let uid = self.getUid()
        var check:Bool = false
        
        
        //CAMPO FRIEND NO USUARIO LOGADO.
        //Verifica de campo friends já foi criada para o usuário.
        self.ref.child(self.getUid()).child("friends").observe(.value, with: { (snapshot) in
            check = snapshot.exists()
            
            if !check {
                //Add amigo no campo friend do usuário logado.
                self.ref.child(uid).child("friends").child(friend.uid!).setValue(friend.toDictionary()) { (error, reference) in
                    
                    guard (error == nil) else {
                        completion(false, error)
                        return;
                    }
                    completion(true, nil)
                }
            }else{
                self.ref.child(uid).child("friends").child(friend.uid!).updateChildValues(friend.toDictionary()) { (error, databseref) in
                    
                    guard error == nil else {
                        completion(false, error)
                        return
                    }
                    completion(true, nil)
                }
            }
        })
    }
    
    func addUserInFriend(_ friend:Friends, objUser:UserModel, completion : @escaping (Bool?, Error?) -> Void) -> Void {
        
        var check2:Bool = false
        let uidF = friend.uid
        
        let objFriend = Friends.init(value: ["accept":false as AnyObject,
                                         "acceptMe":false as AnyObject,
                                         "nome":objUser.name as AnyObject,
                                         "photo":objUser.photo as AnyObject,
                                         "uid":objUser.uid as AnyObject])
        
        //ADD USUARIO LOGADO NO CAMPO FRIEND DO AMIGO.
        self.ref.child(uidF!).child("friends").observe(.value, with: { (snapshot2) in
            check2 = snapshot2.exists()
            
            if check2 {
                
                self.ref.child(uidF!).child("friends").child(objUser.uid!).updateChildValues(objFriend.toDictionary()) { (error, databseref) in
                    guard error == nil else {
                        completion(false, error)
                        return
                    }
                    completion(true, nil)
                }
                
            }else{
                
                self.ref.child(uidF!).child("friends").child(objUser.uid!).setValue(objFriend.toDictionary()) { (error, reference) in
                    guard error == nil else {
                        completion(nil, error)
                        return
                    }
                    completion(true, nil)
                }
                
            }
        })
        
    }
    
    func deleteFriend(_ friendID:String, completion : @escaping (Bool?, Error?) -> Void) -> Void {
        
        let uid = self.getUid()
        
        self.ref.child(uid).child("friends").child(friendID).removeValue { (error, reference) in
            guard error == nil else {
                return completion(false, error)
            }
            self.ref.child(friendID).child("friends").child(uid).removeValue()
            completion(true, nil)
        }
        
    }
    
    func getUserbyUid(_ userID: String, completion: @escaping (UserModel?, Bool, Error?) -> Void) -> Void {
        
        ref.child(userID).observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard snapshot.exists() else {
                completion(nil, true, nil)
                return
            }
            
            if let value = snapshot.value as? NSDictionary {
                
                let user = UserModel.init(value: value as! [String : NSObject])
                completion(user, false, nil);
                return
            }
            
            completion(nil, false, UserError.internalError)
            
        }) { (error) in
            print(error.localizedDescription)
            completion(nil, false, error);
        }
    }
    
    func getOthersUser(_ userID:String, completion: @escaping ([UserModel]?, Error?) -> Void) {
        
        self.ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
            var itens = [UserModel]()
            
            guard snapshot.childrenCount > 0 else {
                completion(itens, nil)
                return
            }
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                if let value = item.value as? NSDictionary {
                    let user = UserModel(value: value as! [String:NSObject])
                    if user.uid != userID {
                        itens.append(user)
                    }
                }
            }
            completion(itens, nil)
        }) { (error) in
            completion(nil, error)
        }
        
    }
    
    private func checkFriend(_ allUserID:String, userID:String) -> Bool {
        var valor : Bool = true
        self.ref.child(userID).child("friends").observeSingleEvent(of: .value, with: { (snapshot) in
            guard snapshot.exists() else {
                return
            }
            
            if let value = snapshot.value as? NSDictionary {
                let friend = Friends.init(value: value as! [String:NSObject])
                if friend.uid != userID {
                    valor = false
                }
            }
        })
        return valor
    }
    
    func createUser(_ userID: String, user: UserModel, completion: @escaping (Bool, Error?) -> Void) -> Void {
        
        self.ref.child(userID).setValue(user.toDictionary()) { (error, reference) in
            
            guard (error == nil) else {
                completion(false, error)
                return;
            }
            
            completion(true, nil)
            
        }
    }
    
    func updateUser(_ key:String!, user: UserModel, completion: @escaping (Bool, Error?) -> Void) -> Void {
        
        self.ref.child(key).updateChildValues(user.toDictionary()) { (error, databseref) in
            
            guard (error == nil) else {
                completion(false, error)
                return
            }
            
            completion(true, nil)
        }
    }
    
    func acceptFriend(_ friendID:String, completion: @escaping (Bool, Error?) -> Void) -> Void {
        
        let uid = self.getUid()
        
        ref.child(uid).child("friends").observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard snapshot.childrenCount > 0 else {
                return
            }
            
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                if let value = item.value as? NSDictionary {
                    let friend = Friends(value: value as! [String:NSObject])
                    if ((friendID == friend.uid) && (!friend.accept!)){
                        completion(false,nil)
                        break;
                    }
                }
            }
            completion(true, nil)
        }) {(error) in
            completion(true, nil)
        }
        
        
    }
    
    func isloggedin() -> Bool{
        guard (Auth.auth().currentUser != nil) else {
            return false
        }
        
        return true
    }
    
    func getCurrentUser(_ completion: @escaping (UserModel?, Bool, Error?) -> Void) -> Void {        
        if let user = Auth.auth().currentUser {
            getUserbyUid(user.uid, completion: completion)
        } else {
            completion(nil, false, LoginError.invalidCredential);
        }
    }
    
    func getUid() -> String {
        return (Auth.auth().currentUser?.uid)!
    }
    
    func updateCountTravels(uid:String, value:Int) {
        self.ref.child(uid).updateChildValues(["count_travels":value])
    }
}
