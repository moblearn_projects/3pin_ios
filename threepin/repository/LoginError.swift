//
//  LoginError.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 10/5/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

enum LoginError : Error {
    case emailAlreadyInUse
    case invalidCredential
    case userDisabled
}
