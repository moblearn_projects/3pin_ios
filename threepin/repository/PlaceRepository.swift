//
//  PlaceRepository.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 10/5/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

import UIKit
import FirebaseDatabase

struct PlaceRepository {
    
    let ref : DatabaseReference;
    let ref_img : DatabaseReference
    
    init() {
        self.ref = Database.database().reference().child("places");
        self.ref_img = Database.database().reference().child("places_images_comment")
    }
    
    func sendCommentPhoto(_ photo:PhotoComment, completion: @escaping(PhotoComment?, Error?) -> Void) -> Void {
        
        let key = ref_img.childByAutoId().key
        let value = photo.toDictionay()
        photo.uid = key
        
        let childUpdates = [key:value]
        self.ref_img.updateChildValues(childUpdates) { (error, databseref) in
            
            guard (error == nil) else {
                completion(nil, error)
                return
            }
            
            completion(photo,nil)
        }
        
    }
    
    func createPlace(_ place: Place, completion: @escaping (Place?, Error?) -> Void) -> Void {
        
        let key = ref.childByAutoId().key
        let value = place.toDictionay()
        place.key = key
        
        let childUpdates = [key:value]
        self.ref.updateChildValues(childUpdates) { (error, databseref) in
            
            guard (error == nil) else {
                completion(nil, error)
                return
            }
            
            completion(place,nil)
        }
    }
    
    func updatePlace(_ key: String!, values: [String:NSObject]!, completion: @escaping (Bool, Error?) -> Void) -> Void {
        
        let childUpdates = [key:values]
        
        self.ref.updateChildValues(childUpdates) { (error, databseref) in
            
            guard (error == nil) else {
                completion(false, error)
                return
            }
            
            completion(true,nil)
        }
    }
    
    func getPhotosComments(_ photoId:String!, completion: @escaping([PhotoComment]?, Error?, _ success:Bool?) -> Void) -> Void {
        
        self.ref_img.observeSingleEvent(of: .value, with: { (snapshot) in
            
            var itens = [PhotoComment]()
            
            guard snapshot.exists() else {
                completion(nil, nil, false)
                return
            }
            
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                if let value = item.value as? NSDictionary {
                    let photoComment = PhotoComment(value: value as! [String : NSObject])
                    if photoComment.id == photoId {
                        itens.append(photoComment)
                    }
                }
            }
            completion(itens, nil, true)
        }){ (error) in
            completion(nil, error, false)
        }
        
    }
    
    func getPlacesTravel(_ travelID:String!, completion: @escaping([Place]?, Error?)->Void) -> Void {
        
        
        self.ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
            var itens = [Place]()
            
            guard snapshot.exists() else {
                completion(nil, nil)
                return
            }
            
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                if let value = item.value as? NSDictionary {
                    let place = Place(value: value as! [String : NSObject])
                    if place.uidTravel == travelID {
                        itens.append(place)
                    }
                }
            }
            completion(itens, nil)
        }){ (error) in
            completion(nil, error)
        }
        
    }
    
    func getPlaces(_ completion: @escaping ([Place]?, Error?) -> Void) -> Void {
        
        self.ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
            var itens = [Place]()
            
            guard snapshot.childrenCount > 0 else {
                completion(itens, nil)
                return
            }
            
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                
                if let value = item.value as? NSDictionary {
                    let place = Place(value: value as! [String : NSObject])
                    itens.append(place)
                }
            }
            
            completion(itens, nil)
        
        }) { (error) in
            completion(nil, error)
        }
    }
    
    /**
     * sempre que um place for recuperado pele KEY sera criado um observe, para monitorar qualquer mudanca
     * dos indicadores de curtidas, comentarios, roteiros, etc. 
     */
    func getPlaceWithKey(_ key: String, completion: @escaping (Place?, Error?) -> Void) -> (DatabaseReference,UInt) {
        
        let refPlace = self.ref.child(key)
        let handle = refPlace.observe(.value, with: { (snapshot) in
            
            guard snapshot.exists() else {
                completion(nil, ModelError.notFound)
                return
            }
            
            if let value = snapshot.value as? NSDictionary {
                let place = Place.init(value: value as! [String : NSObject])
                completion(place, nil)
                return
            }
            
            completion(nil, ModelError.notFound)
            
        }) { (error) in
            completion(nil, error)
        }
        
        return (refPlace, handle);
    }
    
    func getPlaceWithName(_ name: String, completion: @escaping ([Place]?, Error?) -> Void) -> Void {
        
        self.ref.queryOrdered(byChild: "name").queryEqual(toValue: name).observeSingleEvent(of: .value, with: { (snapshot) in
            
            var itens = [Place]();
            
            guard snapshot.childrenCount > 0 else {
                completion(itens, nil)
                return
            }
            
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                
                if let value = item.value as? NSDictionary {
                    let place = Place(value: value as! [String : NSObject])
                    itens.append(place)
                }
            }
            
        }) { (error) in
            completion(nil, error)
        }
    }
    
}
