//
//  TermOfUseError.swift
//  threepin
//
//  Created by Râmede Bento on 17/10/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

import Foundation

enum TermOfUseError : Error {
    case notFound
    case internalError(message:String)
}
