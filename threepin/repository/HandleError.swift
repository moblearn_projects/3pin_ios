//
//  HandleError.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 10/5/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

import FirebaseAuth

struct HandleError {
    
    static func handle(_ error: Error) -> String {
        
        if let userError = error as? UserError {
            
            return handleUserError(userError)
        }
        else if let firAuthError = error as? LoginError {
        
            return handleFirAuthError(firAuthError)
        }
        else if let termOfUserError = error as? TermOfUseError {
            
            return handleTermOfUseError(termOfUserError)
        }
        return error.localizedDescription
    }
    
    fileprivate static func handleTermOfUseError(_ error: TermOfUseError) -> String {
        
        switch error {
            
        case .notFound:
            return "Usuário não encontrado!"
            
        case .internalError(let message):
            return message

            
        }
    }
    
    fileprivate static func handleUserError(_ error: UserError) -> String {
     
        switch error {
            
        case .notFound:
            return "Usuário não encontrado!"
            
        case .internalError:
            return "Ocorreu um problema ao processar os dados do seu usuário, por favor tente novamente."
            
        }
    }
    
    fileprivate static func handleFirAuthError(_ error: LoginError) -> String {
        
        switch error {
        case .emailAlreadyInUse:
            return "Conta informada já está sendo usado por outro usuário"
            
        case .invalidCredential:
            return "Credencias fornecidas são invalidas"
            
        case .userDisabled:
            return "Este usuário está desabilitado, entre em contato com nossa equipe."
        }
    }
    
    static func handleFIRAuthErrorCode(_ error: Error) -> LoginError {
        
        let code = (error as NSError).code
        
        if let errCode = AuthErrorCode(rawValue: code) {
            
            switch errCode {
            case AuthErrorCode.invalidCredential:
                return LoginError.invalidCredential
                
            case AuthErrorCode.operationNotAllowed:
                return LoginError.invalidCredential
                
            case AuthErrorCode.wrongPassword:
                return LoginError.invalidCredential
                
            case AuthErrorCode.emailAlreadyInUse:
                return LoginError.emailAlreadyInUse
                
            case AuthErrorCode.userDisabled:
                return LoginError.userDisabled
                
            default:
                return LoginError.invalidCredential
            }
        }
        
        return LoginError.invalidCredential;
    }
}
