//
//  TermOfUseRepository.swift
//  threepin
//
//  Created by Râmede Bento on 17/10/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase

struct TermOfUseRepository {
    
    let ref : DatabaseReference
    
    init() {
        self.ref = Database.database().reference().child("termsOfUse")
    }

    func createTermId() -> Void {
        let key = ref.childByAutoId().key
        print("###### KEY: \(key)");
    }
    
    func getLastTermOfUse(_ completion: @escaping (TermOfUse, Error?) -> Void) -> Void {

        self.ref.queryOrdered(byChild: "createdDate").observeSingleEvent(of: .value, with: { (snapshot) in
            
            var term = TermOfUse()
            
            let arr = snapshot.children.allObjects as! [DataSnapshot]
            if let value = arr.reversed()[0].value as? NSDictionary {
                term = TermOfUse(value: value as! [String : NSObject])
                completion(term, nil);
                return
            }

            completion(term, TermOfUseError.notFound)
            
        }) { (error) in
            completion(TermOfUse(), error)
        }

    }
    
    func getTermWithKey(_ key: String, completion: @escaping (TermOfUse?, Error?) -> Void) -> (DatabaseReference,UInt) {
        
        let refTerm = self.ref.child(key)
        let handle = refTerm.observe(.value, with: { (snapshot) in
            
            guard snapshot.exists() else {
                completion(nil, TermOfUseError.notFound)
                return
            }
            
            if let value = snapshot.value as? NSDictionary {
                let term = TermOfUse.init(value: value as! [String : NSObject])
                completion(term, nil)
                return
            }
            
            completion(nil, TermOfUseError.notFound)
            
        }) { (error) in
            completion(nil, error)
        }
        
        return (refTerm, handle);
    }
    
}
