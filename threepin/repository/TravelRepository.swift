//
//  TravelRepository.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 27/01/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct TravelRepository {
    let ref : DatabaseReference;
    let refTravelUsers : DatabaseReference
    let refUserTravels : DatabaseReference
    let refImagesTravel: DatabaseReference
    let refLikesTravel: DatabaseReference
    let refFriendsTravel : DatabaseReference
    let refUser: DatabaseReference
    
    init() {
        self.ref = Database.database().reference().child("travels");
        self.refTravelUsers = Database.database().reference().child("travels_users");
        self.refUserTravels = Database.database().reference().child("users_travels")
        self.refImagesTravel = Database.database().reference().child("travels_images");
        self.refLikesTravel = Database.database().reference().child("travels_likes");
        self.refFriendsTravel = Database.database().reference().child("travels_friends");
        self.refUser = Database.database().reference().child("users");
    }
    
    func addFriendTravel(travelID: String, uidFriend: String, completion: @escaping (Bool?, Error?) -> Void) -> Void {
        
        let value = ["uid":uidFriend as AnyObject]
        self.refFriendsTravel.child(travelID).child(uidFriend).setValue(value) { (success, error) in
            guard (error == nil) else {
                completion(false, error.debugDescription as? Error)
                return
            }
            completion(true, nil)
        }
        
    }
    
    func photoIsPrivate(photo:Photo, travelID:String, completion:@escaping(Bool?, Error?) -> Void) -> Void {
        
        self.refImagesTravel.child(travelID).child(photo.id).updateChildValues(photo.toDictionay()){(success, error) in
            
            guard error == nil else {
                completion(false, error.debugDescription as? Error)
                return
            }
            completion(true, nil)
            
        }
        
    }
    
    func removeFriendTravel(travelID:String, uidFriend:String, completion: @escaping (Bool?, Error?) -> Void) -> Void {
        
        self.refTravelUsers.child(uidFriend).child(travelID).removeValue { (error, snapshot) in
            guard error == nil else {
                completion(false, error)
                return
            }
            
            self.refUserTravels.child(travelID).child(uidFriend).removeValue { (error, snapshot) in
                guard error == nil else {
                    completion(false, error)
                    return
                }
                completion(true, nil)
            }
        }
        
        
        
        /*self.refFriendsTravel.child(travelID).child(uidFriend).removeValue { (error, snapshot) in
            guard error == nil else {
                completion(false, error)
                return
            }
        }
        
        completion(true, nil)*/
        
    }    
    
    func createTravelUser(uid:String, travelID:String, isPrivate:Bool, completion: @escaping (Bool?, Error?) -> Void) -> Void {
        let value = ["id":travelID as AnyObject,
                     "isPrivate":isPrivate as AnyObject,
                     "uid":uid as AnyObject]
        
        let value2 = ["uid":uid as AnyObject,
                      "id":travelID as AnyObject,
                      "isPrivate":isPrivate as AnyObject]
        
        self.refTravelUsers.child(uid).child(travelID).setValue(value) { (error, snapshot) in
            guard error == nil else {
                completion(false, error.debugDescription as? Error)
                return
            }
            
            self.refUserTravels.child(travelID).child(uid).setValue(value2, withCompletionBlock: { (error, snapshot2) in
                guard error == nil else {
                    completion(false, error.debugDescription as? Error)
                    return
                }
                completion(true, nil)
            })
            
            
//            completion(true, nil)
        }
        
        /*self.refTravelUsers.child(uid).child(travelID).setValue(value){ (success, error) in
            guard error == nil else {
                completion(false, error.debugDescription as? Error)
                return
            }
            
            completion(true, nil)
        }*/
        
    }
    
    func create(travel: Travel, completion: @escaping (Travel?, Error?) -> Void) -> Void {
        
        var key: String
        var atualizacao:Bool = false
        
        if travel.id != nil {
            key = travel.id!
            atualizacao = true
        }
        else {
            key = ref.childByAutoId().key
            travel.id = key
        }
        
        let value = travel.toDictionay()
        let childUpdates = [key:value]
        
        self.ref.updateChildValues(childUpdates) { (error, databseref) in
            
            guard (error == nil) else {
                completion(nil, error)
                return
            }
            
            if !atualizacao {
                DispatchQueue.main.async(execute: {
                    self.createTravelUser(uid: travel.uid!, travelID: travel.id!, isPrivate: true, completion: { (success, error) in
                        guard error != nil else {
                            return
                        }
                    })
                })
            }
            
            completion(travel,nil)
        }
        
    }
    
    func savePhotos(travel:String, items:Array<Dictionary<String, AnyObject>>, completion: @escaping (Bool?, Error?) -> Void) ->Void {
        
        let refTravel = self.refImagesTravel.child(travel)
        var children: [String:AnyObject] = [:]
        
        for var item in items {
            let id = refTravel.childByAutoId().key
            item["id"] = id as AnyObject?
            children[id] = item as AnyObject?
        }
        
        self.refImagesTravel.child(travel).updateChildValues(children) { (error, databseref) in
            guard (error == nil) else {
                completion(false, error)
                return
            }
            
            completion(true,nil)
        }
        
    }
    
    func removePhotoTravel(photoID:String, travelID:String, completion: @escaping (String?, Error?) -> Void) -> Void {
        
        self.refImagesTravel.child(travelID).child(photoID).removeValue { (error, snapshot) in
            guard error == nil else {
                completion(nil, error)
                return
            }
        }
        
        completion("Removido com sucesso!", nil)
        
    }
    
    func getPeopleTravels(travelID:String, uid:String, completion:@escaping([UserModel]?, Error?) -> Void) -> Void {
        
        let refUT = self.refUserTravels.child(travelID)
        
        refUT.observeSingleEvent(of: .value, with: { (snapshot) in
            
            var objUsuario = [UserModel]()
            
            guard snapshot.childrenCount > 0 else {
                completion(objUsuario, nil)
                return
            }
            
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                if let value = item.value as? NSDictionary {
                    let travelUser = TravelUser(value: value as! [String:NSObject]) //Travel(value: value as! [String:NSObject])
                    if uid != travelUser.uid {
                        objUsuario.append(UserModel(value: ["uid":travelUser.uid as AnyObject]))
                    }
                }
            }
            completion(objUsuario, nil)
        })
        
        
        /*self.refTravelUsers.observeSingleEvent(of: .value, with: { (snapshot) in
            
            var objUsuario = [UserModel]()
            
            guard snapshot.childrenCount > 0 else {
                completion(objUsuario, nil)
                return
            }
            
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                if let value = item.value as? NSDictionary {
                    let travelUser = TravelUser(value: value as! [String:NSObject]) //Travel(value: value as! [String:NSObject])
                    if travelID == travelUser.travelId {
                        objUsuario.append(UserModel(value: ["uid":travelUser.uid as AnyObject]))
                    }
                }


            }
            completion(objUsuario, nil)
        })*/
        
    }
    
    func getPeopleTravel(travelID:String, completion: @escaping ([UserModel]?, Error?) -> Void) -> Void {
        
        var objUsuario = [UserModel]()//Objeto completo
        var objUserIds = [UserModel]()//Objeto travel_friends
        var resultAll  = [UserModel]()
        
        self.refUser.observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard snapshot.childrenCount > 0 else {
                completion(objUsuario, nil)
                return
            }
            
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                if let value = item.value as? NSDictionary {
                    let user = UserModel(value: value as! [String : NSObject])
                    objUsuario.append(user)
                }
            }
        })
        
        self.refFriendsTravel.child(travelID).observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard snapshot.childrenCount > 0 else {
                completion(objUserIds, nil)
                return
            }
            
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                if let value = item.value as? NSDictionary {
                    let user = UserModel(value: value as! [String : NSObject])
                    objUserIds.append(user)
                    
                    for u in objUsuario {
                        if u.uid == user.uid {
                            resultAll.append(u)
                        }
                    }
                    
                }
            }
            
            completion(resultAll, nil)
            
        }) { (error) in
            completion(nil, error)
        }
        
    }
    
    func getTravelsByUsers(_ uid:String, completion:@escaping([Travel]?, Error?) -> Void) -> Void {
        
        let refUserTravel = self.refTravelUsers.child(uid)
        refUserTravel.observeSingleEvent(of: .value, with: { (snapshot) in
            
            var travels = [Travel]()
            
            guard snapshot.childrenCount > 0 else {
                completion(travels, nil)
                return
            }
            
            for travel in snapshot.children.allObjects as! [DataSnapshot] {
                
                if let value = travel.value as? NSDictionary {
                    let objTravel = Travel(value: value as! [String : NSObject])
                    travels.append(objTravel)
                }
            }
            
            completion(travels, nil)
            
        }) { (error) in
            completion(nil, error)
        }
        
    }
    
    func getTravelsByUId(_ uid:String, trip:Bool, completion: @escaping ([Travel]?, Error?) -> Void) {
        
        self.ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
            var itens = [Travel]()
            
            guard snapshot.childrenCount > 0 else {
                completion(itens, nil)
                return
            }
            
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                
                if let value = item.value as? NSDictionary {
                    let travel = Travel(value: value as! [String : NSObject])
                    if trip {
                        if travel.uid == uid {
                            itens.append(travel)
                        }
                    }else{
                        itens.append(travel)
                    }                    
                }
            }
            
            completion(itens, nil)
            
        }) { (error) in
            completion(nil, error)
        }
    }
    
    
    /**
     * sempre que uma viagem for recuperada pelo ID sera criado um observe, para monitorar qualquer mudanca
     * dos indicadores de curtidas, comentarios, roteiros, etc.
     */
    func getTravelWithID(_ key: String, completion: @escaping (Travel?, Error?) -> Void) -> (DatabaseReference,UInt) {
        
        let refTravel = self.ref.child(key)
        let handle = refTravel.observe(.value, with: { (snapshot) in
            
            guard snapshot.exists() else {
                completion(nil, ModelError.notFound)
                return
            }
            
            if let value = snapshot.value as? NSDictionary {
                let place = Travel.init(value: value as! [String : NSObject])
                completion(place, nil)
                return
            }
            
            completion(nil, ModelError.notFound)
            
        }) { (error) in
            completion(nil, error)
        }
        
        return (refTravel, handle);
    }
    
    func getTravelByID(_ key: String, completion: @escaping (Travel?, Error?) -> Void) -> Void {
        
        let refTravel = self.ref.child(key)
        refTravel.observe(.value, with: { (snapshot) in
            
            guard snapshot.exists() else {
                completion(nil, ModelError.notFound)
                return
            }
            
            if let value = snapshot.value as? NSDictionary {
                let place = Travel.init(value: value as! [String : NSObject])
                completion(place, nil)
                return
            }
            
            completion(nil, ModelError.notFound)
            
        }) { (error) in
            completion(nil, error)
        }
    }
    
    func observePhotosWithTravelID(_ key:String, completion: @escaping (Array<Photo>?, Error?) -> Void) -> (DatabaseReference,UInt){
        
        let refAlbum = self.refImagesTravel.child(key)
        let handle = refAlbum.observe(.value, with: { (snapshot) in
            
            var itens = [Photo]()
            
            guard snapshot.childrenCount > 0 else {
                completion(itens, nil)
                return
            }
            
            for item in snapshot.children.allObjects as! [DataSnapshot] {
                
                if let value = item.value as? NSDictionary {
                    let photo = Photo(value: value as! [String : NSObject])
                    if photo.uid != AppSession.sharedInstance.uid {
                        if !photo.isPrivate {
                            itens.append(photo)
                        }
                    }else{
                        itens.append(photo)
                    }
                    
                }
            }
            
            completion(itens, nil)
            
        })
        
        return (refAlbum, handle)
        
    }
    
    func like(_ travel:Travel, userID:String, info:Dictionary<String, AnyObject>) {
        
        refLikesTravel.child(travel.id!).child(userID).setValue(info)

        refUser.child(userID).child("likes").child("travels")
            .child(travel.id!).setValue(["id":travel.id])
    }
    
    func unLike(_ travel:Travel, userID:String) {
        refLikesTravel.child(travel.id!).child(userID).removeValue()

        refUser.child(userID).child("likes").child("travels")
            .child(travel.id!).removeValue()
    }

}
