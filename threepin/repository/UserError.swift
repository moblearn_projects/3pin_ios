//
//  ErrorHandling.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 10/4/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

enum UserError : Error {
    case notFound
    case internalError
}
