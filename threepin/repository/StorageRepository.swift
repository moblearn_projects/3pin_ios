//
//  StorageRepository.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 1/26/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit
import Firebase
import Crashlytics

struct StorageRepository {
    
    let storageRef: StorageReference
    
    init() {
        storageRef = Storage.storage().reference(forURL: "gs://pin-30c31.appspot.com")
    }
    
    func saveProfile(name: String, file: URL, completion: @escaping (String?, Error?) -> Void) -> Void {
        let refFile = storageRef.child("profile/"+name)
        upload(file: file, ref: refFile, completion: completion);
    }
    
    func deleteProfile(name:String, completion: @escaping (String?, Error?) -> Void) -> Void {
        let refFile = storageRef.child("profile/"+name)
        delete(ref: refFile, completion: completion)
    }
    
    func deleteTravels(name:String, completion:@escaping(String?, Error?) -> Void) -> Void {
        let refFile = storageRef.child("travels/travel_"+name)
        delete(ref: refFile, completion: completion)
    }
    
    func saveTravel(name: String, file: URL, completion: @escaping (String?, Error?) -> Void) -> Void {
        let refFile = storageRef.child("travels/"+name)
        upload(file: file, ref: refFile, completion: completion);
    }
    
    func saveBackground(name: String, file: URL, completion: @escaping (String?, Error?) -> Void) -> Void {
        let refFile = storageRef.child("background/"+name)
        upload(file: file, ref: refFile, completion: completion);
    }
    
    private func upload(file: URL, ref: StorageReference, completion: @escaping (String?, Error?) -> Void) -> Void {
        
        ref.putFile(from: file, metadata: nil) { (metadata, error) in
        
            guard (error == nil) else {
                completion(nil, error);
                return;
            }
            
            let url = metadata!.downloadURL()
            try! completion(String(contentsOf: url!), nil);            
        }
    }
    
    func saveProfile(name: String, data: Data, completion: @escaping (String?, Error?) -> Void) -> Void {
        let refFile = storageRef.child("profile/"+name)
        upload(data: data, ref: refFile, completion: completion);
    }
    
    func saveTravel(name: String, data: Data, completion: @escaping (String?, Error?) -> Void) -> Void {
        let refFile = storageRef.child("travels/"+name)
        upload(data: data, ref: refFile, completion: completion);
    }
    
    func savePlace(name: String, data: Data, completion: @escaping (String?, Error?) -> Void) -> Void {
        let refFile = storageRef.child("places/"+name)
        upload(data: data, ref: refFile, completion: completion);
    }
    
    func saveBackground(name: String, data: Data, completion: @escaping (String?, Error?) -> Void) -> Void {
        let refFile = storageRef.child("background/"+name)
        upload(data: data, ref: refFile, completion: completion);
    }
    
    private func delete(ref:StorageReference, completion: @escaping (String?, Error?)->Void) -> Void {
        
        ref.delete { (error) in
            guard (error == nil) else {
                completion(nil, error);
                Crashlytics.sharedInstance().recordError(error!)
                return
            }
            completion("Deletado com sucesso!", nil)
        }
        
    }
    
    private func upload(data: Data, ref: StorageReference, completion: @escaping (String?, Error?) -> Void) -> Void {
        
        ref.putData(data, metadata: nil) { (metadata, error) in
            guard (error == nil) else {
                completion(nil, error);
                Crashlytics.sharedInstance().recordError(error!)
                return;
            }
            
            let url = metadata!.downloadURL()
            completion(url?.absoluteString, nil);

        }        
    }
}
