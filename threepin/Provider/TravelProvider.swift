//
//  TravelProvider.swift
//  threepin
//
//  Created by Anderson Silva on 27/11/2017.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

class TravelProvider : NSObject {
    
    func getDataGoogle(q:String, completion:@escaping(_ base:GoogleSearchBaseClass?, _ error:String?) -> Void) -> Void {
        
        let url = "https://www.googleapis.com/customsearch/v1?key=AIzaSyALxLH3CWvU5bX8qkruuBYQwAbM18R1RaM&cx=009355924464565206621:fdvbed8_-ds&q=\(q)&searchType=image"
        
        request(url).responseObject { (response:DataResponse<GoogleSearchBaseClass>) in
            guard response.response?.statusCode == 200 else {
                completion(nil, response.response.debugDescription)
                return
            }
            
            guard let base = response.result.value else {
                completion(nil, response.response.debugDescription)
                return
            }
            
            completion(base, nil)
            
        }
        
    }
    
}
