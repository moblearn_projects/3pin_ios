//
//  TravelBusiness.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 27/01/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit
import FirebaseDatabase

struct TravelBusiness{
    
    let repository: TravelRepository;
    let userRepository: UserRepository;
    let storage: StorageRepository;
    
    var observeTravel:(DatabaseReference,UInt)? = nil//DatabaseReference? //(FIRDatabaseReference,UInt)? = nil
    var observePhotos:(DatabaseReference,UInt)? = nil//DatabaseReference? //(FIRDatabaseReference,UInt)? = nil
    weak var baseViewDelegate : BaseViewControllerDelegate?
    
    init() {
        self.repository = TravelRepository.init()
        self.userRepository = UserRepository.init()
        self.storage = StorageRepository.init()
    }
    
    func createTravelUser(uid:String, travelID:String, isPrivate:Bool, completion: @escaping (Bool?, Error?) -> Void) -> Void {
        self.repository.createTravelUser(uid: uid, travelID: travelID, isPrivate: isPrivate) { (success, error) in
            guard error == nil else {
                completion(success, error)
                return
            }
            completion(success, nil)
        }
    }
    
    func addFriendTravel(travelID: String, uidFriend: String, completion: @escaping (Bool?, Error?) -> Void) -> Void {
        self.repository.addFriendTravel(travelID: travelID, uidFriend: uidFriend) { (success, error) in
            guard error == nil else {
                completion(false, error?.localizedDescription as? Error)
                return
            }
            completion(true, nil)
        }
    }
    
    func removeFriendTravel(travelID:String, uidFriend:String, completion: @escaping (Bool?, Error?) -> Void) -> Void {
        self.repository.removeFriendTravel(travelID: travelID, uidFriend: uidFriend) { (success, error) in
            guard success! else {
                completion(false, error)
                return
            }
            completion(success, nil)
        }
    }
    
    func create(_ travel: Travel, completion: @escaping (Travel?, Error?) -> Void) -> Void {
        
        travel.uid = userRepository.getUid()
        
        if travel.id == nil {
            userRepository.getCurrentUser({ (user, value, error) in
                guard user != nil else {
                    return
                }
                user?.travels += 1
                self.userRepository.updateCountTravels(uid: (user?.uid)!, value: (user?.travels)!)
            })
        }
        
        repository.create(travel: travel, completion: { (result, error) in
            //TODO: Tratar error
            guard (error == nil) else {
                completion(nil, error)
                return
            }
            
            completion(result,nil)
        })
    }
    
    func uploadPhotos(_ image:[UIImage], completion: @escaping ([String]?, Error?) -> Void) {
        
        if image.count > 0 {
            var urlRetorno = [String]()
            for img in image {
                self.uploadPhoto(img, completion: { (url, error) in
                    guard error == nil else {
                        completion(nil, error)
                        return
                    }
                    urlRetorno.append(url!)
                    if urlRetorno.count == image.count {
                        completion(urlRetorno, nil)
                    }
                })
            }
        }        
    }
    
    func uploadPhoto (_ image: UIImage, completion: @escaping (String?, Error?) -> Void) {
        
        let uid = userRepository.getUid()
        let name = String(format: "travel_%@_%d.jpg", uid, getCurrentMillis())
        let data = UIImageJPEGRepresentation(image, 0.7)
        
        storage.saveTravel(name: name, data: data!, completion: completion)
        
    }
    
    func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    func savePhotos(photos:Array<Photo>, travel:String, completion: @escaping (Bool?, Error?) -> Void) -> Void {
        
        var items = Array<Dictionary<String, AnyObject>>();
        
        for photo in photos {
            if(photo.id == nil) {
                photo.travel = travel
                photo.uid = userRepository.getUid()
                items.append(photo.toDictionay())
            }
        }
        repository.savePhotos(travel: travel, items: items, completion: completion);
        items.removeAll()
    }
    
    func privatePhoto(photos:[Photo], travelID:String, completion: @escaping(Bool?, Error?) -> Void) -> Void {
        
        for ph in photos {
            
            if !ph.isPrivate {
                ph.isPrivate = true
            }else{
                ph.isPrivate = false
            }
            
            self.privatePhotoTravel(photo: ph, travelID: travelID, completion: { (success, error) in
                guard error == nil else {
                    completion(false, error)
                    return
                }
            })
            
        }
        completion(true, nil)
    }
    
    private func privatePhotoTravel(photo:Photo, travelID:String, completion: @escaping(Bool?, Error?) -> Void) -> Void {
        
        self.repository.photoIsPrivate(photo: photo, travelID: travelID) { (success, error) in
            guard error == nil else {
                completion(false, error)
                return
            }
            completion(true, nil)
        }
        
    }
    
    func removePhotos(photos:[Photo], travelID:String, completion: @escaping (String?, Error?) -> Void) -> Void {
        
        if photos.count > 0 {
            for ph in photos {
                
                self.removePhotoTravel(photoID: ph.id, travelID: travelID, completion: { (success, error) in
                    guard error == nil else {
                        completion(nil, error)
                        return
                    }
                    
                })
                
            }
            completion("Imagens removida(s) com sucesso!", nil)
        }
        
    }
    
    func removePhotoTravel(photoID:String, travelID:String, completion: @escaping (String?, Error?) -> Void) -> Void {
        self.repository.removePhotoTravel(photoID: photoID, travelID: travelID) { (success, error) in
            guard error == nil else {
                completion(nil, error)
                return
            }
            completion(success, nil)
        }
    }
    
    func getPeopleTravels(travelID:String, uid:String, completion:@escaping([UserModel]?, Error?) -> Void) -> Void {
        
        self.repository.getPeopleTravels(travelID: travelID, uid: uid) { (user, error) in
            var userObj = [UserModel]()
            guard user != nil else {
                completion(nil, error)
                return
            }
            
            for usuario in user! {
                self.userRepository.getUserbyUid(usuario.uid!, completion: { (objUser, success, error) in
                    guard objUser != nil else {
                        completion(nil, error)
                        return
                    }
                    userObj.append(objUser!)
                    if user!.count == userObj.count {
                        completion(userObj,nil)
                    }
                })
            }
        }
        
    }
    
    func getPeopleTravel(travelID:String, completion: @escaping ([UserModel]?, Error?) -> Void) -> Void {
        
        self.repository.getPeopleTravel(travelID: travelID) { (user, error) in
            guard user != nil else {
                completion(nil, error)
                return
            }
            completion(user,nil)
        }
    }
    
    func getTravelsByUsers(_ uid:String, completion:@escaping([Travel]?, Error?) -> Void) -> Void {
        
        repository.getTravelsByUsers(uid) { (travels, error) in
            
            var objTravels = [Travel]()
            
            guard travels != nil else {
                completion(nil, error)
                return
            }
            
            for value in travels! {
                
                self.repository.getTravelByID(value.id!, completion: { (travel, error) in
                    guard error == nil else {
                        completion(nil, error)
                        return
                    }
                    objTravels.append(travel!)
                    
                    if objTravels.count == travels?.count {
                        completion(objTravels, nil)
                    }
                    
                })
                
            }            
            
        }
        
    }
    
    func getTravelsByUId(_ uid:String, trip:Bool = false, completion: @escaping ([Travel]?, Error?) -> Void) {
        
       repository.getTravelsByUId(uid,trip: trip) { (result, error) in
        guard (error == nil) else {
            //TODO: tratar error
            completion(nil, error)
            return
        }
        
        completion(result, nil)
        }
    }
    
    mutating func getTravelWithID(_ key: String, completion: @escaping (Travel?, Error?) -> Void) {
        
        self.observeTravel = repository.getTravelWithID(key) { (travel, error) in
            completion(travel, error)
        }
    }
    
    func like(_ travel:Travel) {
        
        travel.likes += 1
        userRepository.getCurrentUser { (user, value, error) in
            guard user != nil else{
                return
            }
            
            let userArray = ["uid":user?.uid, "name":user?.name,"photo":user?.photo]
            self.repository.like(travel, userID:(user?.uid)!, info:userArray as Dictionary<String, AnyObject>)
        }
    }
    
    
    func unLike(_ travel:Travel) {
        
        travel.likes -= 1
        let currentUser = AppSession.sharedInstance.currentUser
        
        repository.unLike(travel, userID:(currentUser?.uid)!)
    }
    
    mutating func observePhotosWithTravelID(_ key:String, completion: @escaping (Array<Photo>?, Error?) -> Void) {
        self.observePhotos = repository.observePhotosWithTravelID(key, completion: completion)
    }
    
    /**
     * esse metodo sempre deve ser executado quando o viewDidDisappear da ViewController for acionado
     */
    func removeObserve() -> Void {
        
        if let observe = observeTravel {
            
            let ref = observe.0
            let handle = observe.1
            
            ref.removeObserver(withHandle: handle)
        }
        
        if let observe = observePhotos {
            
            let ref = observe.0
            let handle = observe.1
            
            ref.removeObserver(withHandle: handle)
        }
    }    
}
