//
//  TermOFUseBusiness.swift
//  threepin
//
//  Created by Râmede Bento on 17/10/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

import UIKit
import FirebaseDatabase

struct TermOfUseBusiness {
    
    let repository: TermOfUseRepository;
    var observeTermOfUse: (DatabaseReference,UInt)? = nil //DatabaseReference? //(FIRDatabaseReference,UInt)? = nil
    
    init() {
        self.repository = TermOfUseRepository.init()
    }

    func createTermId(){
        repository.createTermId()
    }
    
    func getLastTermOfUse(_ completion: @escaping (TermOfUse, String?) -> Void) -> Void {
        
        repository.getLastTermOfUse() { (term, error) in
            
            guard (error == nil) else {
                let message = HandleError.handle(error!)
                completion(TermOfUse(), message)
                return;
            }
            
            completion(term, nil)
        }
        
    }
    
    /**
     * esse metodo sempre deve ser executado quando o viewDidDisappear da ViewController for acionado
     */
    func removeObservePlace() -> Void {
        
        if let observe = observeTermOfUse {
            
            let ref = observe.0
            let handle = observe.1

            ref.removeObserver(withHandle: handle)
        }
    }
    
}
