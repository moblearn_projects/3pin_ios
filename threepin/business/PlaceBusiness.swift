//
//  PlaceBusines.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 10/5/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

struct PlaceBusiness {
    
    let repository: PlaceRepository;
    var storeage: StorageRepository;
    var observePlace:(DatabaseReference,UInt)? = nil
    
    init() {
        self.repository = PlaceRepository.init()
        self.storeage = StorageRepository.init()
    }
    
    func sendCommentPhoto(_ photo:PhotoComment, completion: @escaping(PhotoComment?, Error?) -> Void) -> Void {
        
        repository.sendCommentPhoto(photo) { (result, error) in
            //TODO: Tratar error
            guard (error == nil) else {
                completion(nil, error)
                return
            }
            
            completion(photo,nil)
        }
        
    }
    
    func getPhotosComments(_ photoId:String!, completion: @escaping([PhotoComment]?, Error?, _ success:Bool?) -> Void) -> Void {
        
        repository.getPhotosComments(photoId) { (photoComment, error, success) in
            guard (error == nil) else {
                completion(nil, error, success)
                return
            }
            completion(photoComment,nil, success)            
        }
        
    }
        
    func createPlace(_ place: Place, completion: @escaping (Place?, Error?) -> Void) -> Void {
        
        if place.imagem != nil {
            
            self.uploadPhoto(place.imagem!, completion: { (url, error) in
                guard error == nil else {
                    completion(nil, error)
                    return
                }
                place.photo = url!
                
                self.repository.createPlace(place) { (result, error) in
                    
                    //TODO: Tratar error
                    guard (error == nil) else {
                        completion(nil, error)
                        return
                    }
                    completion(place,nil)
                }
                
            })
            
        }else{
        
            repository.createPlace(place) { (result, error) in
                
                //TODO: Tratar error
                guard (error == nil) else {
                    completion(nil, error)
                    return
                }
                
                completion(place,nil)
            }
        }
    }
    
    func updatePlace(_ key: String!, values: [String:NSObject]!, completion: @escaping (Bool, Error?) -> Void) -> Void {
        
        repository.updatePlace(key, values: values) { (result, error) in
            
            //TODO: Tratar error
            guard (error == nil) else {
                completion(false, error)
                return
            }
            
            completion(true, nil)
        }
    }
    
    func uploadPhoto (_ image: UIImage, completion: @escaping (String?, Error?) -> Void) {
        
        let name = "photo_" + (Auth.auth().currentUser?.uid)! + ".jpg"
//        let name = "photo_" + travelID + "_" + photoID + ".jpg"
        let data = UIImageJPEGRepresentation(image, 0.7)
        
        storeage.savePlace(name: name, data: data!, completion: completion)
        
    }
    
    func getPlacesTravel(_ travelID:String!, completion: @escaping([Place]?, Error?)->Void) -> Void {
        
        repository.getPlacesTravel(travelID) { (places, error) in
            
            guard places != nil else {
                completion(nil, error)
                return
            }
            
            completion(places, nil)
            
        }
        
    }
    
    
    func getPlaces(_ completion: @escaping ([Place]?, Error?) -> Void) -> Void {
        
        repository.getPlaces { (places, error) in
            
            //TODO: Tratar error
            guard (error == nil) else {
                completion(nil, error)
                return
            }
            
            completion(places, nil)
        }    
    }
    /**
     * esse metodo sempre deve ser executado quando o viewDidDisappear da ViewController for acionado
     */
    func removeObservePlace() -> Void {
        
        if let observe = observePlace {
            
            let ref = observe.0
            let handle = observe.1
            
            ref.removeObserver(withHandle: handle)
        }
    }
    
}
