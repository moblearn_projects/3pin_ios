//
//  TravelNegocios.swift
//  threepin
//
//  Created by Anderson Silva on 27/11/2017.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import Foundation

protocol TravelInterface : class {
    func updateImages(total:Int, itens:[GoogleSearchItems])
}

class TravelNegocios : NSObject {
    
    private lazy var provider:TravelProvider = {
        return TravelProvider.init()
    }()
    
    weak var interface : TravelInterface?
    weak var baseViewInterface : BaseViewControllerDelegate?
    
    func getDataGoogle(q:String) {
        self.baseViewInterface?.loadingShow()
        provider.getDataGoogle(q: q) { (base, error) in
            self.baseViewInterface?.loadingHide()
            guard error == nil else {
                self.baseViewInterface?.alertMsg(title: "Ops!", msg: error!, btn: "OK")
                return
            }
            
            if base!.items!.count > 0 {
                
                self.interface?.updateImages(total: base!.items!.count, itens: (base?.items!)!)
                
            }
            
        }
        
    }
    
}
