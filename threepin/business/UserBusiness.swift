//
//  UserBusines.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 10/4/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import FirebaseAuth
import FirebaseDatabase

struct UserBusiness {
    
    var repository: UserRepository;
    var storeage: StorageRepository;
    var observeUser:(DatabaseReference,UInt)? = nil
    
    init() {
        self.repository = UserRepository.init()
        self.storeage = StorageRepository.init()
    }
    
    
    
    func login(_ token: String, completion: @escaping (UserModel? , Bool) -> Void, errorBlock: @escaping (String?) -> Void) -> Void {
        
        repository.loginWithFacebook(token) { (firUser, error) in
            
            guard (error == nil) else {
                errorBlock(HandleError.handle(error!))
                return
            }
        
            self.getUserWithUid((firUser?.uid)!, completion: { (user, isNewuser, messageError) in

                guard (messageError == nil) else {
                    errorBlock(messageError)
                    return
                }
                
                completion(user, isNewuser)
                
            })
        }
    }
    
    func getUserWithUid(_ userID: String, completion: @escaping (UserModel?, Bool, String?) -> Void) -> Void {
        
        repository.getUserbyUid(userID) { (user, isNewUser, error) in
            
            guard (error == nil) else {
                
                let message = HandleError.handle(error!)
            
                completion(nil, false, message)
            
                return;
            }
            
            completion(user, isNewUser, nil)
        }
    }
    
    func createUser(user: UserModel, completion: @escaping (Bool, String?) -> Void) -> Void {
        
        let uid = repository.getUid()
        user.uid = uid
        
        repository.createUser(uid, user: user) { (sucess, error) in
            
            guard (error == nil) else {
                let message = HandleError.handle(error!)
                completion(false, message)
                return
            }
            
            completion(true, nil)
        }
    }
    
    func updateUser(user:UserModel, completion: @escaping (Bool, Error?) -> Void) -> Void {
        
        let uid = repository.getUid()
        user.uid = uid
        
        repository.updateUser(uid, user: user) { (result, error) in
            guard (error == nil) else {
                completion(false, error)
                return
            }
            
            completion(true, nil)
        }        
    }
    
    func loginWithFacebook(_ viewController: LoginViewController, completion: @escaping (UserModel?, Bool) -> Void, errorBlock: @escaping (String?) -> Void) -> Void {
        
        let loginManager = FBSDKLoginManager.init()
        
        loginManager.logIn(withReadPermissions: ["public_profile", "email"], from: viewController) { (result, error) in
        
            guard (error == nil) else {
                errorBlock(error?.localizedDescription)
                return
            }
            
            guard !(result?.isCancelled)! else {
                errorBlock("Login cancelado pelo usuario")
                return
            }
            
            let token = FBSDKAccessToken.current().tokenString
            
            self.login(token!, completion: completion, errorBlock: errorBlock)
        }
        
    }
    
    func isloggedin() -> Bool{
        return repository.isloggedin();
    }
    
    static func getUidCurrent() -> String{
        
        if let uid = Auth.auth().currentUser?.uid {
            return uid
        }
        
        return ""
    }
    
    func getOthersUser(_ userID:String, completion: @escaping ([UserModel]?, Error?) -> Void) {
        
        repository.getOthersUser(userID) { (user, error) in
            guard error == nil else {
                completion(nil, error)
                return
            }            
            completion(user, nil)
        }
        
    }
    
    func getFriendsCollection(_ userID:String, completion : @escaping ([Friends]?, Error?) -> Void) -> Void {
        
        repository.getFriendsCollection(userID) { (friends, error) in
            guard error == nil else {
                completion(nil, error)
                return
            }
            completion(friends, nil)
        }
        
    }
    
    func getFriends(_ userID:String, completion : @escaping ([Friends]?, Error?) -> Void) -> Void {
        
        repository.getFriends(userID) { (friends, error) in
            guard error == nil else {
                completion(nil, error)
                return
            }
            completion(friends, nil)
        }
        
    }
    
    func removePeople(uid:String, idFriend:String, completion : @escaping (Bool?, Error?) -> Void) -> Void {
        repository.removePeople(uid: uid, idFriend: idFriend) { (success, error) in
            guard error == nil else {
                completion(false, error?.localizedDescription as? Error)
                return
            }
            completion(true, nil)
        }
    }
    
    func acceptFriendMe(_ friend:Friends, completion : @escaping (Bool?, Error?) -> Void) -> Void {
        
        repository.acceptFriendMe(friend) { (success, error) in
            guard error == nil else {
                completion(false, error)
                return
            }
            
            completion(true, nil)
        }
        
    }
    
    func acceptMeFriend(_ friend:Friends, objUser:UserModel, completion : @escaping (Bool?, Error?) -> Void) -> Void {
        
        repository.acceptMeFriend(friend, objUser: objUser) { (success, error) in
            guard error == nil else {
                completion(false, error)
                return
            }
            
            completion(true, nil)
        }
        
    }
    
    func countFriendsPendent(_ userID:String) -> Int {
        
        let total = repository.countFriendsPendent(userID)
        
        return total
        
//        repository.getFriendsPendent(userID) { (friends, error) in
//            guard error == nil else {
//                return
//            }
//            total = friends!.count
//        }
//        
//        return total
        
    }
    
    func getFriendsPendent(_ userID:String, completion : @escaping ([Friends]?, Error?) -> Void) -> Void {
        repository.getFriendsPendent(userID) { (friends, error) in
            guard error == nil else {
                completion(nil, error)
                return
            }
            completion(friends, nil)
        }
    }
    
    func acceptFriend(_ friendID:String, completion: @escaping (Bool, Error?) -> Void) -> Void {
        
        repository.acceptFriend(friendID) { (success, error) in
            guard error == nil else {
                completion(true, error)
                return
            }
            
            completion(success, nil)
        }
        
    }
    
    func addFriend(_ friend:Friends, completion : @escaping (Bool?, Error?) -> Void) -> Void {
        
        repository.addFriend(friend) { (success, error) in
            
            guard error == nil else {
                completion(false, error)
                return
            }
            completion(true, nil)
        }
    }
    
    func addUserInFriend(_ friend:Friends, objUser:UserModel, completion : @escaping (Bool?, Error?) -> Void) -> Void {
        
        repository.addUserInFriend(friend,objUser: objUser) { (success, error) in
            guard error == nil else {
                completion(false, error)
                return
            }
            completion(success, nil)
        }
        
    }
    
    func deleteFriend(_ friendID:String, completion : @escaping (Bool?, Error?) -> Void) -> Void {
        
        repository.deleteFriend(friendID) { (success, error) in
            guard error == nil else {
                completion(false, error)
                return 
            }
            completion(true, nil)
        }
    }
    
    func getCurrentUser(_ completion: @escaping (UserModel?, Bool, String?) -> Void) -> Void {
        
        repository.getCurrentUser { (user, isNewUser, error) in
            guard (error == nil) else {
                
                let message = HandleError.handle(error!)
                
                completion(nil, false, message)
                
                return;
            }
            
            completion(user, isNewUser, nil)
        }
    }
    
    func uploadPhoto (_ image: UIImage, completion: @escaping (String?, Error?) -> Void) {
        
        let name = "photo_" + (Auth.auth().currentUser?.uid)! + ".jpg"
        let data = UIImageJPEGRepresentation(image, 0.7)
    
        storeage.saveProfile(name: name, data: data!, completion: completion)
        
    }
    
    func deletePhoto(completion: @escaping (String?, Error?) -> Void) {
        let name = "photo_" + (Auth.auth().currentUser?.uid)! + ".jpg"
        
        storeage.deleteProfile(name: name, completion: completion)
    }
    
    func uploadCover (_ image: UIImage, completion: @escaping (String?, Error?) -> Void) {
        
        let name = "cover_" + (Auth.auth().currentUser?.uid)! + ".jpg"
        let data = UIImageJPEGRepresentation(image, 0.7)
        
        storeage.saveBackground(name: name, data: data!, completion: completion)
        
    }
    
    func plusTravels(user:UserModel) {
        user.travels += 1
        repository.updateCountTravels(uid: user.uid!, value: user.travels)
    }
    
    func minusTravels(user:UserModel) {
        user.travels -= 1
        repository.updateCountTravels(uid: user.uid!, value: user.travels)
    }
    
    func removeObserveUser() -> Void {
        
        if let observe = observeUser {
            
            let ref = observe.0
            let handle = observe.1
            
            ref.removeObserver(withHandle: handle)
        }
    }
    
}
