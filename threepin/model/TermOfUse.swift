//
//  TermOfUse.swift
//  threepin
//
//  Created by Râmede Bento on 17/10/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

import UIKit

class TermOfUse: NSObject {
    
    var key: String
    var name: String
    var content: String

    init(value: [String:NSObject?]) {
        self.key = value["key"] as! String
        self.name = value["name"] as! String
        self.content = value["content"] as! String
    }
    
    convenience override init(){
        let val = ["key":"","name":"","content":""] as [String : Any]
        self.init(value:val as! [String : NSObject])
    }
    
    func toDictionay() -> [String:NSObject] {
        return [
            "key":key as NSObject,
            "name":name as NSObject,
            "content":content as NSObject,
        ]
    }
    
}
