//
//  PhotoComment.swift
//  threepin
//
//  Created by Anderson Silva on 05/11/2017.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import Foundation
import UIKit

class PhotoComment {
    
    var id: String!
    var url: String!
    var comments: String!
    var travel: String!
    var uid: String!
    var nameUser:String!
    
    //MARK: - Initializers
    init(value: [String:AnyObject]) {
        
        id = value["id"] as! String?
        url = value["url"] as! String?
        comments = value["comments"] as! String?
        travel = value["travel"] as! String?
        uid = value["uid"] as! String?
        nameUser = value["name"] as! String?
    }
    
    convenience init() {
        self.init(value: [String:AnyObject]())
    }
    
    convenience init(url:String) {
        self.init(value: ["url":url as AnyObject])
    }
    
    func toDictionay() -> [String:AnyObject] {
        return  [
            "id": (id ?? "") as AnyObject,
            "url":url as AnyObject,
            "comments": comments as AnyObject,
            "travel": travel as AnyObject,
            "uid:": uid as AnyObject,
            "name": nameUser as AnyObject
        ]
    }
}

