//
//  TravelFriends.swift
//  threepin
//
//  Created by Anderson Silva on 30/11/2017.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import Foundation

import Foundation

class TravelFriends {
    
    
    
    var id: String?
    var name: String?
    var cover: String?
    var startDate: Int64?
    var endDate: Int64?
    var destiny: String?
    var uid: String?
    var isPrivate: Bool
    var permissao:String?
    var likes:Int
    
    //MARK: - Initializers
    init(value: [String:AnyObject]) {
        
        id = value["id"] as! String?
        name = value["name"] as! String?
        cover = value["cover"] as! String?
        startDate = value["startDate"] as! Int64?
        endDate = value["endDate"] as! Int64?
        destiny = value["destiny"] as! String?
        uid = value["uid"] as! String?
        isPrivate = (value["isPrivate"] as! Bool?) ?? false
        permissao = value["permissao"] as! String?
        likes = (value["count_likes"] as! Int?) ?? 0
        
    }
    
    convenience init() {
        self.init(value: [String:NSObject]())
    }
    
    func toDictionay() -> [String:AnyObject] {
        
        var item = [
            "id":id as AnyObject,
            "name":name as AnyObject,
            "startDate":startDate as AnyObject,
            "endDate":endDate as AnyObject,
            "destiny":destiny as AnyObject,
            "permissao":permissao as AnyObject,
            "uid":uid as AnyObject
        ]
        
        item["cover"] = (cover ?? nil) as AnyObject
        item["isPrivate"] = isPrivate as AnyObject
        item["count_likes"] = likes as AnyObject
        
        return item
    }
    
}
