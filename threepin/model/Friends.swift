//
//  Friends.swift
//  threepin
//
//  Created by Anderson Silva on 29/05/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit

class Friends {
    
    var name: String?
    var uid: String?
    var photo: String?
    var accept:Bool?
    var acceptMe:Bool?
    
    init(value: [String:AnyObject]) {
        
        self.name = value["nome"] as! String?
        self.uid = value["uid"] as! String?
        self.photo = value["photo"] as! String?
        self.accept = value["accept"] as! Bool?
        self.acceptMe = value["acceptMe"] as! Bool?
    }
    
    func toDictionary() -> [String:AnyObject] {
        
        return  [
            "nome": name as AnyObject,
            "uid": uid as AnyObject,
            "photo": photo as AnyObject,
            "accept":accept as AnyObject,
            "acceptMe":acceptMe as AnyObject
        ]
    }
    
}

