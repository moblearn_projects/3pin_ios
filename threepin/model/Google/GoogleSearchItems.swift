//
//  GoogleSearchItems.swift
//
//  Created by Anderson Silva on 27/11/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class GoogleSearchItems: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let link = "link"
    static let kind = "kind"
    static let mime = "mime"
    static let image = "image"
    static let htmlTitle = "htmlTitle"
    static let title = "title"
    static let displayLink = "displayLink"
    static let htmlSnippet = "htmlSnippet"
    static let snippet = "snippet"
  }

  // MARK: Properties
  public var link: String?
  public var kind: String?
  public var mime: String?
  public var image: GoogleSearchImage?
  public var htmlTitle: String?
  public var title: String?
  public var displayLink: String?
  public var htmlSnippet: String?
  public var snippet: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    link <- map[SerializationKeys.link]
    kind <- map[SerializationKeys.kind]
    mime <- map[SerializationKeys.mime]
    image <- map[SerializationKeys.image]
    htmlTitle <- map[SerializationKeys.htmlTitle]
    title <- map[SerializationKeys.title]
    displayLink <- map[SerializationKeys.displayLink]
    htmlSnippet <- map[SerializationKeys.htmlSnippet]
    snippet <- map[SerializationKeys.snippet]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = link { dictionary[SerializationKeys.link] = value }
    if let value = kind { dictionary[SerializationKeys.kind] = value }
    if let value = mime { dictionary[SerializationKeys.mime] = value }
    if let value = image { dictionary[SerializationKeys.image] = value.dictionaryRepresentation() }
    if let value = htmlTitle { dictionary[SerializationKeys.htmlTitle] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = displayLink { dictionary[SerializationKeys.displayLink] = value }
    if let value = htmlSnippet { dictionary[SerializationKeys.htmlSnippet] = value }
    if let value = snippet { dictionary[SerializationKeys.snippet] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.link = aDecoder.decodeObject(forKey: SerializationKeys.link) as? String
    self.kind = aDecoder.decodeObject(forKey: SerializationKeys.kind) as? String
    self.mime = aDecoder.decodeObject(forKey: SerializationKeys.mime) as? String
    self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? GoogleSearchImage
    self.htmlTitle = aDecoder.decodeObject(forKey: SerializationKeys.htmlTitle) as? String
    self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
    self.displayLink = aDecoder.decodeObject(forKey: SerializationKeys.displayLink) as? String
    self.htmlSnippet = aDecoder.decodeObject(forKey: SerializationKeys.htmlSnippet) as? String
    self.snippet = aDecoder.decodeObject(forKey: SerializationKeys.snippet) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(link, forKey: SerializationKeys.link)
    aCoder.encode(kind, forKey: SerializationKeys.kind)
    aCoder.encode(mime, forKey: SerializationKeys.mime)
    aCoder.encode(image, forKey: SerializationKeys.image)
    aCoder.encode(htmlTitle, forKey: SerializationKeys.htmlTitle)
    aCoder.encode(title, forKey: SerializationKeys.title)
    aCoder.encode(displayLink, forKey: SerializationKeys.displayLink)
    aCoder.encode(htmlSnippet, forKey: SerializationKeys.htmlSnippet)
    aCoder.encode(snippet, forKey: SerializationKeys.snippet)
  }

}
