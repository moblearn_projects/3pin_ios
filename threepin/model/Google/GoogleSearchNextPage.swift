//
//  GoogleSearchNextPage.swift
//
//  Created by Anderson Silva on 27/11/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class GoogleSearchNextPage: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let startIndex = "startIndex"
    static let outputEncoding = "outputEncoding"
    static let totalResults = "totalResults"
    static let cx = "cx"
    static let searchType = "searchType"
    static let count = "count"
    static let title = "title"
    static let searchTerms = "searchTerms"
    static let safe = "safe"
    static let inputEncoding = "inputEncoding"
  }

  // MARK: Properties
  public var startIndex: Int?
  public var outputEncoding: String?
  public var totalResults: String?
  public var cx: String?
  public var searchType: String?
  public var count: Int?
  public var title: String?
  public var searchTerms: String?
  public var safe: String?
  public var inputEncoding: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    startIndex <- map[SerializationKeys.startIndex]
    outputEncoding <- map[SerializationKeys.outputEncoding]
    totalResults <- map[SerializationKeys.totalResults]
    cx <- map[SerializationKeys.cx]
    searchType <- map[SerializationKeys.searchType]
    count <- map[SerializationKeys.count]
    title <- map[SerializationKeys.title]
    searchTerms <- map[SerializationKeys.searchTerms]
    safe <- map[SerializationKeys.safe]
    inputEncoding <- map[SerializationKeys.inputEncoding]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = startIndex { dictionary[SerializationKeys.startIndex] = value }
    if let value = outputEncoding { dictionary[SerializationKeys.outputEncoding] = value }
    if let value = totalResults { dictionary[SerializationKeys.totalResults] = value }
    if let value = cx { dictionary[SerializationKeys.cx] = value }
    if let value = searchType { dictionary[SerializationKeys.searchType] = value }
    if let value = count { dictionary[SerializationKeys.count] = value }
    if let value = title { dictionary[SerializationKeys.title] = value }
    if let value = searchTerms { dictionary[SerializationKeys.searchTerms] = value }
    if let value = safe { dictionary[SerializationKeys.safe] = value }
    if let value = inputEncoding { dictionary[SerializationKeys.inputEncoding] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.startIndex = aDecoder.decodeObject(forKey: SerializationKeys.startIndex) as? Int
    self.outputEncoding = aDecoder.decodeObject(forKey: SerializationKeys.outputEncoding) as? String
    self.totalResults = aDecoder.decodeObject(forKey: SerializationKeys.totalResults) as? String
    self.cx = aDecoder.decodeObject(forKey: SerializationKeys.cx) as? String
    self.searchType = aDecoder.decodeObject(forKey: SerializationKeys.searchType) as? String
    self.count = aDecoder.decodeObject(forKey: SerializationKeys.count) as? Int
    self.title = aDecoder.decodeObject(forKey: SerializationKeys.title) as? String
    self.searchTerms = aDecoder.decodeObject(forKey: SerializationKeys.searchTerms) as? String
    self.safe = aDecoder.decodeObject(forKey: SerializationKeys.safe) as? String
    self.inputEncoding = aDecoder.decodeObject(forKey: SerializationKeys.inputEncoding) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(startIndex, forKey: SerializationKeys.startIndex)
    aCoder.encode(outputEncoding, forKey: SerializationKeys.outputEncoding)
    aCoder.encode(totalResults, forKey: SerializationKeys.totalResults)
    aCoder.encode(cx, forKey: SerializationKeys.cx)
    aCoder.encode(searchType, forKey: SerializationKeys.searchType)
    aCoder.encode(count, forKey: SerializationKeys.count)
    aCoder.encode(title, forKey: SerializationKeys.title)
    aCoder.encode(searchTerms, forKey: SerializationKeys.searchTerms)
    aCoder.encode(safe, forKey: SerializationKeys.safe)
    aCoder.encode(inputEncoding, forKey: SerializationKeys.inputEncoding)
  }

}
