//
//  GoogleSearchSearchInformation.swift
//
//  Created by Anderson Silva on 27/11/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class GoogleSearchSearchInformation: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let searchTime = "searchTime"
    static let formattedSearchTime = "formattedSearchTime"
    static let formattedTotalResults = "formattedTotalResults"
    static let totalResults = "totalResults"
  }

  // MARK: Properties
  public var searchTime: Float?
  public var formattedSearchTime: String?
  public var formattedTotalResults: String?
  public var totalResults: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    searchTime <- map[SerializationKeys.searchTime]
    formattedSearchTime <- map[SerializationKeys.formattedSearchTime]
    formattedTotalResults <- map[SerializationKeys.formattedTotalResults]
    totalResults <- map[SerializationKeys.totalResults]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = searchTime { dictionary[SerializationKeys.searchTime] = value }
    if let value = formattedSearchTime { dictionary[SerializationKeys.formattedSearchTime] = value }
    if let value = formattedTotalResults { dictionary[SerializationKeys.formattedTotalResults] = value }
    if let value = totalResults { dictionary[SerializationKeys.totalResults] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.searchTime = aDecoder.decodeObject(forKey: SerializationKeys.searchTime) as? Float
    self.formattedSearchTime = aDecoder.decodeObject(forKey: SerializationKeys.formattedSearchTime) as? String
    self.formattedTotalResults = aDecoder.decodeObject(forKey: SerializationKeys.formattedTotalResults) as? String
    self.totalResults = aDecoder.decodeObject(forKey: SerializationKeys.totalResults) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(searchTime, forKey: SerializationKeys.searchTime)
    aCoder.encode(formattedSearchTime, forKey: SerializationKeys.formattedSearchTime)
    aCoder.encode(formattedTotalResults, forKey: SerializationKeys.formattedTotalResults)
    aCoder.encode(totalResults, forKey: SerializationKeys.totalResults)
  }

}
