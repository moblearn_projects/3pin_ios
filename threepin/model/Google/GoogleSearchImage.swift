//
//  GoogleSearchImage.swift
//
//  Created by Anderson Silva on 27/11/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class GoogleSearchImage: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let height = "height"
    static let byteSize = "byteSize"
    static let thumbnailWidth = "thumbnailWidth"
    static let contextLink = "contextLink"
    static let width = "width"
    static let thumbnailHeight = "thumbnailHeight"
    static let thumbnailLink = "thumbnailLink"
  }

  // MARK: Properties
  public var height: Int?
  public var byteSize: Int?
  public var thumbnailWidth: Int?
  public var contextLink: String?
  public var width: Int?
  public var thumbnailHeight: Int?
  public var thumbnailLink: String?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    height <- map[SerializationKeys.height]
    byteSize <- map[SerializationKeys.byteSize]
    thumbnailWidth <- map[SerializationKeys.thumbnailWidth]
    contextLink <- map[SerializationKeys.contextLink]
    width <- map[SerializationKeys.width]
    thumbnailHeight <- map[SerializationKeys.thumbnailHeight]
    thumbnailLink <- map[SerializationKeys.thumbnailLink]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = height { dictionary[SerializationKeys.height] = value }
    if let value = byteSize { dictionary[SerializationKeys.byteSize] = value }
    if let value = thumbnailWidth { dictionary[SerializationKeys.thumbnailWidth] = value }
    if let value = contextLink { dictionary[SerializationKeys.contextLink] = value }
    if let value = width { dictionary[SerializationKeys.width] = value }
    if let value = thumbnailHeight { dictionary[SerializationKeys.thumbnailHeight] = value }
    if let value = thumbnailLink { dictionary[SerializationKeys.thumbnailLink] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.height = aDecoder.decodeObject(forKey: SerializationKeys.height) as? Int
    self.byteSize = aDecoder.decodeObject(forKey: SerializationKeys.byteSize) as? Int
    self.thumbnailWidth = aDecoder.decodeObject(forKey: SerializationKeys.thumbnailWidth) as? Int
    self.contextLink = aDecoder.decodeObject(forKey: SerializationKeys.contextLink) as? String
    self.width = aDecoder.decodeObject(forKey: SerializationKeys.width) as? Int
    self.thumbnailHeight = aDecoder.decodeObject(forKey: SerializationKeys.thumbnailHeight) as? Int
    self.thumbnailLink = aDecoder.decodeObject(forKey: SerializationKeys.thumbnailLink) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(height, forKey: SerializationKeys.height)
    aCoder.encode(byteSize, forKey: SerializationKeys.byteSize)
    aCoder.encode(thumbnailWidth, forKey: SerializationKeys.thumbnailWidth)
    aCoder.encode(contextLink, forKey: SerializationKeys.contextLink)
    aCoder.encode(width, forKey: SerializationKeys.width)
    aCoder.encode(thumbnailHeight, forKey: SerializationKeys.thumbnailHeight)
    aCoder.encode(thumbnailLink, forKey: SerializationKeys.thumbnailLink)
  }

}
