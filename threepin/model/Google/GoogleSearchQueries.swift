//
//  GoogleSearchQueries.swift
//
//  Created by Anderson Silva on 27/11/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class GoogleSearchQueries: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let nextPage = "nextPage"
    static let request = "request"
  }

  // MARK: Properties
  public var nextPage: [GoogleSearchNextPage]?
  public var request: [GoogleSearchRequest]?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    nextPage <- map[SerializationKeys.nextPage]
    request <- map[SerializationKeys.request]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = nextPage { dictionary[SerializationKeys.nextPage] = value.map { $0.dictionaryRepresentation() } }
    if let value = request { dictionary[SerializationKeys.request] = value.map { $0.dictionaryRepresentation() } }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.nextPage = aDecoder.decodeObject(forKey: SerializationKeys.nextPage) as? [GoogleSearchNextPage]
    self.request = aDecoder.decodeObject(forKey: SerializationKeys.request) as? [GoogleSearchRequest]
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(nextPage, forKey: SerializationKeys.nextPage)
    aCoder.encode(request, forKey: SerializationKeys.request)
  }

}
