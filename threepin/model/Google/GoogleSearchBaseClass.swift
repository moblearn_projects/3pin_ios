//
//  GoogleSearchBaseClass.swift
//
//  Created by Anderson Silva on 27/11/2017
//  Copyright (c) . All rights reserved.
//

import Foundation
import ObjectMapper

public final class GoogleSearchBaseClass: Mappable, NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let searchInformation = "searchInformation"
    static let kind = "kind"
    static let items = "items"
    static let queries = "queries"
    static let context = "context"
    static let url = "url"
  }

  // MARK: Properties
  public var searchInformation: GoogleSearchSearchInformation?
  public var kind: String?
  public var items: [GoogleSearchItems]?
  public var queries: GoogleSearchQueries?
  public var context: GoogleSearchContext?
  public var url: GoogleSearchUrl?

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public required init?(map: Map){

  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    searchInformation <- map[SerializationKeys.searchInformation]
    kind <- map[SerializationKeys.kind]
    items <- map[SerializationKeys.items]
    queries <- map[SerializationKeys.queries]
    context <- map[SerializationKeys.context]
    url <- map[SerializationKeys.url]
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = searchInformation { dictionary[SerializationKeys.searchInformation] = value.dictionaryRepresentation() }
    if let value = kind { dictionary[SerializationKeys.kind] = value }
    if let value = items { dictionary[SerializationKeys.items] = value.map { $0.dictionaryRepresentation() } }
    if let value = queries { dictionary[SerializationKeys.queries] = value.dictionaryRepresentation() }
    if let value = context { dictionary[SerializationKeys.context] = value.dictionaryRepresentation() }
    if let value = url { dictionary[SerializationKeys.url] = value.dictionaryRepresentation() }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.searchInformation = aDecoder.decodeObject(forKey: SerializationKeys.searchInformation) as? GoogleSearchSearchInformation
    self.kind = aDecoder.decodeObject(forKey: SerializationKeys.kind) as? String
    self.items = aDecoder.decodeObject(forKey: SerializationKeys.items) as? [GoogleSearchItems]
    self.queries = aDecoder.decodeObject(forKey: SerializationKeys.queries) as? GoogleSearchQueries
    self.context = aDecoder.decodeObject(forKey: SerializationKeys.context) as? GoogleSearchContext
    self.url = aDecoder.decodeObject(forKey: SerializationKeys.url) as? GoogleSearchUrl
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(searchInformation, forKey: SerializationKeys.searchInformation)
    aCoder.encode(kind, forKey: SerializationKeys.kind)
    aCoder.encode(items, forKey: SerializationKeys.items)
    aCoder.encode(queries, forKey: SerializationKeys.queries)
    aCoder.encode(context, forKey: SerializationKeys.context)
    aCoder.encode(url, forKey: SerializationKeys.url)
  }

}
