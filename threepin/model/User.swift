//
//  User.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 10/4/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

import UIKit

class UserModel {
    
    var name: String?
    var address: String?
    var uid: String?
    var country: String?
    var state: String?
    var city: String?
    var gender: String?
    var dateOfBirth: String?
    var phone: String?
    var schooling: String?
    var occupation: String?
    var income: String?
    var maritalStatus: String?
    var photo: String?
    var cover: String?
    var travels: Int
//    var friend: Friends?
    
    init(value: [String:AnyObject]) {
        
        self.name = value["name"] as! String?
        self.uid = value["uid"] as! String?
        self.address = value["address"] as! String?
        self.country = value["country"] as! String?
        self.state = value["state"] as! String?
        self.city = value["city"] as! String?
        self.gender = value["gender"] as! String?
        self.dateOfBirth = value["dateOfBirth"] as! String?
        self.schooling = value["schooling"] as! String?
        self.occupation = value["occupation"] as! String?
        self.income = value["income"] as! String?
        self.maritalStatus = value["maritalStatus"] as! String?
        self.phone = value["phone"] as! String?
        self.cover = value["cover"] as! String?
        self.photo = value["photo"] as! String?
        self.travels = (value["count_travels"] as! Int?) ?? 0
//        self.friend  = value["friends"] as! Friends?
    }
    
    convenience init() {
        self.init(value: [String:AnyObject]())
    }
    
    func toDictionary() -> [String:AnyObject] {
        
        return  [
            "name": name as AnyObject,
            "uid": uid as AnyObject,
            "address": address as AnyObject,
            "country": country as AnyObject,
            "state": state as AnyObject,
            "city": city as AnyObject,
            "gender": gender as AnyObject,
            "dateOfBirth": dateOfBirth as AnyObject,
            "schooling": schooling as AnyObject,
            "occupation": occupation as AnyObject,
            "income": income as AnyObject,
            "maritalStatus": maritalStatus as AnyObject,
            "phone": phone as AnyObject,
            "cover": cover as AnyObject,
            "photo": photo as AnyObject,
            "count_travels": travels as AnyObject
        ]
    }
    
}
