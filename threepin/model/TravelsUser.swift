//
//  TravelsUser.swift
//  threepin
//
//  Created by Anderson Silva on 30/11/2017.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import Foundation

class TravelUser {
    
    var travelId: String?
    var isPrivate: Bool
    var uid:String?
    
    //MARK: - Initializers
    init(value: [String:AnyObject]) {
        
        travelId = value["travelId"] as! String?
        isPrivate = (value["isPrivate"] as! Bool?) ?? false
        uid = value["uid"] as! String?
        
    }
    
    convenience init() {
        self.init(value: [String:NSObject]())
    }
    
    func toDictionay() -> [String:AnyObject] {
        
        var item = ["travelId":travelId as AnyObject]
        item["isPrivate"] = isPrivate as AnyObject
        item["uid"] = uid as AnyObject
        
        return item
    }
    
}

