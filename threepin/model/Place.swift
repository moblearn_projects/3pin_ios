//
//  Place.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 10/5/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

import UIKit

class Place {
    
    var key: String?
    var name: String?
    var countTrips: Int?
    var address: String?
    var countLikes: Int?
    var countComments: Int?
    var countStars: Int?
    var photo: String?
    var uidTravel:String?
    var latitude:Double?
    var longitude:Double?
    var imagem : UIImage?
    var url : URL?
    
    //MARK: - Initializers
    init(value: [String:AnyObject]) {
        
        key = value["key"] as! String?
        name = value["name"] as! String?
        countTrips = value["countTrips"] as! Int?
        address = value["address"] as! String?
        countLikes = value["countLikes"] as! Int?
        countComments = value["countComments"] as! Int?
        countStars = value["countStars"] as! Int?
        photo = value["photo"] as! String?
        uidTravel = value["uidTravel"] as! String?
        latitude = value["latitude"] as! Double?
        longitude = value["longitude"] as! Double?
    }
    
    convenience init() {
        self.init(value: [String:AnyObject]())
    }
    
    func toDictionay() -> [String:AnyObject] {
        return [
            "key":key as AnyObject,
            "name":name as AnyObject,
            "countTrips":countTrips as AnyObject,
            "address":address as AnyObject,
            "countLikes":countLikes as AnyObject,
            "countComments":countComments as AnyObject,
            "countStars":countStars as AnyObject,
            "photo":photo as AnyObject,
            "uidTravel":uidTravel as AnyObject,
            "latitude":latitude as AnyObject,
            "longitude":longitude as AnyObject
        ]
    }
    
}
