//
//  Photo.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 02/02/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import Foundation
import UIKit

class Photo {
    
    var id: String!
    var url: String!
    var likes: Int!
    var comments: Int!
    var travel: String!
    var isPrivate: Bool!
    var uid: String!
    var image:UIImage?
    var selecionar:Bool?
    
    //MARK: - Initializers
    init(value: [String:AnyObject]) {
        
        id = value["id"] as! String?
        url = value["url"] as! String?
        comments = value["count_comments"] as! Int?
        travel = value["travel"] as! String?
        isPrivate = value["isPrivate"] as! Bool?
        likes = value["count_likes"] as! Int?
        uid = value["uid"] as! String?
    }
    
    convenience init() {
        self.init(value: [String:AnyObject]())
    }
    
    convenience init(url:String) {
        self.init(value: ["url":url as AnyObject])
    }
    
    func toDictionay() -> [String:AnyObject] {
        
        var item =  [
            "url":url as AnyObject,
            "travel": travel as AnyObject,
            "uid": uid as AnyObject
        ]
        
        item["id"] = (id ?? "") as AnyObject
        item["count_comments"] = (comments ?? 0) as AnyObject
        item["isPrivate"] = (isPrivate ?? false) as AnyObject
        item["count_likes"] = (likes ?? 0) as AnyObject

        return item
    }
}
