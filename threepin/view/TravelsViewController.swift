//
//  TravelsViewController.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 31/01/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit

class TravelsViewController: UITableViewController {
    
    @IBOutlet var tbView: UITableView!
    @IBOutlet weak var btnTrip: UIBarButtonItem!
    
    var travels :[Travel]!
    var travelBusiness: TravelBusiness!
    var userBusiness: UserBusiness!
    var allFriendsPendents = [Friends]()
    var uid:String!
    var home:Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userBusiness = UserBusiness.init()
        if home {
            self.uid = self.userBusiness?.repository.getUid()
        }
        travelBusiness = TravelBusiness.init();
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getFriendsPendent(userID: self.uid)
        
        if !home {
            addBackModal()
        }
        
        travelBusiness.getTravelsByUsers(uid) { (travels, error) in
            guard travels != nil else {
                return
            }
            
            if !self.home {
                self.travels = travels?.filter({ $0.permissao == "PÚBLICA" })
            }else{
                self.travels = travels
            }
            
            self.tbView.tableFooterView = UIView()
            self.tbView.reloadData()
        }
    }
    
    func getFriendsPendent(userID:String){
        
        self.userBusiness.getFriendsPendent(userID) { (friend, error) in
            guard friend != nil else {
                return
            }
            
            self.allFriendsPendents = friend!
            if self.allFriendsPendents.count > 0 {
                self.tabBarController?.tabBar.items?[1].badgeValue = "\(self.allFriendsPendents.count)"
            }else{
                self.tabBarController?.tabBar.items?[1].badgeValue = nil
            }
        }
    }
    
    func addBackModal() {
        let barButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self , action: #selector(TravelsViewController.backButtonModal))
        barButton.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = barButton
        navigationItem.rightBarButtonItem = nil
    }
    
    func backButtonModal() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        travelBusiness.removeObserve()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = travels?.count {
            return count
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let travel = travels[indexPath.row]
        let cell:TravelTableViewCell
        
        if travel.cover == nil {
            cell = tbView.dequeueReusableCell(withIdentifier: "cell_travel_empty", for: indexPath) as! TravelTableViewCell
        }else {
            cell = tbView.dequeueReusableCell(withIdentifier: "cell_travel_full", for: indexPath) as! TravelTableViewCell
        }
        
        cell.setData(travels[indexPath.row])
        let viewSeparatorLine = UIView(frame:CGRect(x: 0, y: cell.contentView.frame.size.height - 5.0, width: cell.contentView.frame.size.width, height: 5))
//        viewSeparatorLine.backgroundColor = .red
        cell.contentView.addSubview(viewSeparatorLine)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let travel = travels[indexPath.row]
        self.performSegue(withIdentifier: "open_detail", sender: travel)
    }
    
    @IBAction func btnTrip(_ sender: Any) {
        self.performSegue(withIdentifier: "segueNewTrip", sender: nil)
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if ("open_detail" == segue.identifier) {
            let viewController = segue.destination as! DetailTravelViewController
            viewController.travel = sender as? Travel
        }
    }
    

}
