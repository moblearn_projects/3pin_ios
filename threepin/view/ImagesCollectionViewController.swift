//
//  ImagesCollectionViewController.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 10/02/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit
import Photos
import Reachability

private let reuseIdentifier = "item_photo"

class ImagesCollectionViewController: BaseViewController {
    
    @IBOutlet weak var addImg: UIButton!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet weak var viewExcluir: UIView!
    @IBOutlet weak var btnEye: UIButton!
    @IBOutlet weak var btnTrash: UIButton!
    
    var travel:Travel?
    var travelBusiness:TravelBusiness!
    var photos = [Photo]()
    var photosImg = [Photo]()
    var photosExcluir = [Photo]()
    var isCover:Bool = false
    var imagesBG:[UIImage] = []
    var urlS : [String]?
    var reachability = Reachability()
    var selected:Bool = false
    var btnAtivar:Bool = false


    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBackButton()
        self.title = "Imagens"
        travelBusiness = TravelBusiness.init()
        self.userBusiness = UserBusiness.init()
        if travel?.uid != self.userBusiness.repository.getUid() {
            self.navigationItem.rightBarButtonItem = nil
        }
        self.collectionView.delegate = self
        self.collectionView?.allowsSelection = false
        
        if (reachability?.isReachableViaWiFi)! {
            self.downloadImages()
        }else{
            self.recuperaFotos()
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewExcluir.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        travelBusiness.removeObserve()
        if self.photosExcluir.count > 0 {
            self.photosExcluir.removeAll()
        }
    }
    
    //MARK: Actions
    @IBAction func addImg(_ sender: Any) {
//        openImagePicker()
        
        if !self.btnAtivar {
            ativarView(ativar: true)
        }else{
            if self.photosExcluir.count > 0 {
                self.photosExcluir.removeAll()
            }
            ativarView(ativar: false)
        }
        
//        if !self.selected {
//            ativarView(ativar: true)
//            return
//        }
//        ativarView(ativar: false)
    }
    
    @IBAction func ativarPrivado(_ sender: Any) {
        if self.photosExcluir.count > 0 {
            if self.photosExcluir.count == 1 {
                self.setPrivateImages(photosArray: self.photosExcluir)
            }else{
                self.alertMsg(title: "Ops!", msg: "Favor selecione uma imagem por vez.", btn: "OK")
            }
        }else{
            self.alertMsg(title: "Ops!", msg: "Favor selecione uma imagem.", btn: "OK")
        }
    }
    
    
    @IBAction func excluirImagens(_ sender: Any) {
        if self.photosExcluir.count > 0 {
            self.callRemoveImage(photosArray: self.photosExcluir)
        }else{
            self.alertMsg(title: "Ops!", msg: "Favor selecione uma imagem para excluir.", btn: "OK")
        }
    }
    
    //MARK: Métodos
    func ativarView(ativar:Bool) {
        self.btnAtivar = ativar
        self.selected = ativar
        self.viewExcluir.isHidden = true
        if ativar {
            self.viewExcluir.isHidden = false
        }else {
            self.collectionView.reloadData()
        }
    }
    
    func setPrivateImages(photosArray:[Photo]) {
        
        if photosArray.count > 0 {
            
            self.travelBusiness.privatePhoto(photos: photosArray, travelID: (self.travel?.id)!, completion: { (sucess, error) in
                guard error == nil else {
                    self.alertMsg(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                    return
                }
            })
            self.photosExcluir.removeAll()
            self.btnEye.setImage(UIImage(named: "eye"), for: .normal)
        }
        
    }
    
    func callRemoveImage(photosArray:[Photo]) {
        
        let alertController = UIAlertController(title: "3Pin informa", message: "Você deseja excluir está(s) foto(s)?", preferredStyle: .actionSheet)
        
        let cancel = UIAlertAction(title: "Cancelar", style: .cancel) { (action) in
            
        }
        
        let excluir = UIAlertAction(title: "Excluir", style: .default) { (action) in
            self.showLoading()
            self.travelBusiness.removePhotos(photos: photosArray, travelID: (self.travel?.id)!, completion: { (success, error) in
                self.hideLoading()
                guard error == nil else {
                    self.alertMsg(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                    return
                }
                self.photosExcluir.removeAll()
                self.collectionView.reloadData()
            })

        }
        
        alertController.addAction(cancel)
        excluir.setValue(UIColor.red, forKey: "titleTextColor")
        alertController.addAction(excluir)
        present(alertController, animated: true, completion: nil)
        
    }
    
    func recuperaFotos() {
        
        travelBusiness.observePhotosWithTravelID((travel?.id)!) { (photos, error) in
            guard error == nil else {
                return
            }
            self.collectionView?.allowsSelection = true
            self.photos.removeAll()
            self.photos = photos!
            self.collectionView?.reloadData()

        }
        
    }
    
    func recuperarFotoBiblioteca() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        
        let startDate = Date.init(milliseconds: (travel?.startDate!)!)
        let endDate = Date.init(milliseconds: (travel?.endDate!)!)
        
        let startDateString = String(format: "%@", startDate.dateString(format: "dd-MM-yyyy"))
        let endDateString   = String(format: "%@", endDate.dateString(format: "dd-MM-yyyy"))
        
        fetchPhotosInRange(startDate: formatter.date(from: startDateString)! as NSDate, endDate: formatter.date(from: endDateString)! as NSDate)
        
    }
    
    func fetchPhotosInRange(startDate:NSDate, endDate:NSDate) {
        
        let imgManager = PHImageManager.default()
        
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        requestOptions.isNetworkAccessAllowed = true
        
        // Fetch the images between the start and end date
        let fetchOptions = PHFetchOptions()
        fetchOptions.predicate = NSPredicate(format: "creationDate >= %@ AND creationDate <= %@", startDate, endDate)
        
        imagesBG = []
        
        let fetchResult: PHFetchResult = PHAsset.fetchAssets(with: PHAssetMediaType.image, options: fetchOptions)
        let totalfetchResult = fetchResult.count
        // If the fetch result isn't empty,
        // proceed with the image request
        if fetchResult.count > 0 {
            
            let photo = Photo.init()
            
            // Perform the image request
            for index in 0  ..< fetchResult.count  {
                
                let photoTeste = Photo.init()
                
                let asset = fetchResult.object(at: index)
                imgManager.requestImageData(for: asset, options: requestOptions, resultHandler: { (imageData: Data?, dataUTI: String?, orientation: UIImageOrientation, info: [AnyHashable : Any]?) -> Void in
                    if let imageData = imageData {
                        if let image = UIImage(data: imageData) {
                            // Add the returned image to your array
                            self.imagesBG += [image]
                            
                            //REALIZAR TESTE
                            photoTeste.image = image
                            self.photos.append(photoTeste)

                        }
                    }
                    if self.imagesBG.count == fetchResult.count {
                        // Do something once all the images
                        // have been fetched. (This if statement
                        // executes as long as all the images
                        // are found; but you should also handle
                        // the case where they're not all found.)
                        print("TOTAL IMAGENS: \(totalfetchResult)")
//                        self.photos = self.photosImg
                        self.collectionView?.reloadData()
                        
                        DispatchQueue.main.async(execute: {
                            
                            self.travelBusiness.uploadPhotos(self.imagesBG, completion: { (urlArray, error) in
                                guard error == nil else {
                                    self.travelBusiness.baseViewDelegate?.alertMsg(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                                    return
                                }
                                self.urlS = urlArray!
                                
                                if self.urlS?.count == fetchResult.count {
                                    for valorUrl in self.urlS! {
                                        photo.url = valorUrl
                                        self.photosImg.append(photo)
                                        self.savePhotos((self.travel?.id)!)
                                        self.photosImg.removeAll()
                                    }
                                    self.recuperaFotos()
                                }
                                
                            })
                            
                        })
                        
                    }
                })
            }
        }else {
            self.recuperaFotos()            
        }
    }
    
    func savePhotos(_ key:String) {
        
        travelBusiness.savePhotos(photos: self.photosImg, travel:key) { (success, error) in
            guard success == true else {
                //TODO: tratar error
                return (self.travelBusiness.baseViewDelegate?.alertMsg(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK"))!
            }
        }
    }
    
    func openImagePicker() -> Void {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
//        imagePicker.mediaTypes = [kUTTypeImage as String]
        imagePicker.allowsEditing = true
        
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    func downloadImages() {
        
        let alert = UIAlertController(title: "3Pind", message: "Deseja importar sua bibliotecas de fotos?", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Sim", style: .default) { (action) in
            self.recuperarFotoBiblioteca()
        }
        
        let cancel = UIAlertAction(title: "Não", style: .cancel, handler: { (action) in
            self.self.recuperaFotos()
        })
        alert.addAction(ok)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
        
        if ("open_image_detail" == segue.identifier) {
            let viewController = segue.destination as! DetailImageViewController
            viewController.photo = (sender as! Photo)
        }
    }
    
}

extension ImagesCollectionViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PhotoCollectionViewCell
        cell.inputData((photos[indexPath.row]))

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! PhotoCollectionViewCell
        if self.selected {
            cell.imgSelecionadoExibir()
            var excluido:Bool = false
            if self.photosExcluir.count > 0 {
                
                for (index,ph) in self.photosExcluir.enumerated() {
                    
                    if ph.id == self.photos[indexPath.row].id && ph.selecionar! {
                        cell.imgSelecionadoRemover()
                        excluido = true
                        self.photosExcluir.remove(at: index)
                        break
                    }
                    
                }
                
                if !excluido {
                    photos[indexPath.row].selecionar = true
                    self.photosExcluir.append(photos[indexPath.row])
                }
                
                
            }else{
                photos[indexPath.row].selecionar = true
                self.photosExcluir.append(photos[indexPath.row])
            }
            
            if self.photosExcluir.count == 1 {
                self.btnEye.isEnabled = true
                if photos[indexPath.row].isPrivate {
                    self.btnEye.setImage(UIImage(named: "blue-eye"), for: .normal)
                }else{
                    self.btnEye.setImage(UIImage(named: "eye"), for: .normal)
                }
            }else{
                self.btnEye.isEnabled = false
                self.btnEye.setImage(UIImage(named: "eye"), for: .disabled)
            }
            
            
        }else{
            if self.photosExcluir.count > 0 {
                cell.imgSelecionadoRemover()
            }
            self.performSegue(withIdentifier: "open_image_detail", sender: self.photos[indexPath.row])
        }
    }
    
}

extension ImagesCollectionViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        var imageSelected:UIImage?
        
        if let editImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            imageSelected = editImage
            
        }else if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            imageSelected = pickedImage
        }
        
        
        if let image = imageSelected {
            
            var photo:Photo!
            
            
            self.travelBusiness.baseViewDelegate?.loadingShow()
            self.travelBusiness.baseViewDelegate?.viewDisable(enable: false)
            travelBusiness.uploadPhoto(image, completion: { (url, error) in
                
                if error == nil {
                    
                    photo = Photo.init()
                    photo.image = image
                    photo.isPrivate = false
                    photo.uid = AppSession.sharedInstance.uid
                    photo.url = url
                    self.photosImg.append(photo)
                    
                    self.savePhotos((self.travel?.id)!)
                    self.travelBusiness.baseViewDelegate?.loadingHide()
                    self.travelBusiness.baseViewDelegate?.viewDisable(enable: true)
                }else{
                    self.travelBusiness.baseViewDelegate?.alertMsg(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                    self.travelBusiness.baseViewDelegate?.loadingHide()
                    self.travelBusiness.baseViewDelegate?.viewDisable(enable: true)
                }
                self.dismiss(animated: true, completion: nil)
            })
        }
        
        
        
    }
    
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
}
