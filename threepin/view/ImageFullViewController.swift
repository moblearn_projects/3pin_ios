//
//  ImageFullViewController.swift
//  threepin
//
//  Created by Anderson Silva on 25/01/2018.
//  Copyright © 2018 Moblearn. All rights reserved.
//

import UIKit
import Kingfisher

class ImageFullViewController: BaseViewController {

    var url : String?
    
    @IBOutlet weak var imgF: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.downGestureModal()
        self.imgF.kf.setImage(with: URL(string: url!))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
