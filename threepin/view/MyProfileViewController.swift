//
//  MyProfileViewController.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 07/02/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Kingfisher

class MyProfileViewController: BaseViewController {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnTravels: UIButton!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var btSair: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var addFriend: UIButton!
    @IBOutlet weak var addTrip: UIButton!
    
    var uid:String!
    var friends = [Friends]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addFriend.addShaddows()
        addTrip.addShaddows()
        btnTravels.isHidden = true
        self.userBusiness = UserBusiness.init()
        
        self.uid = self.userBusiness?.repository.getUid()
        
        self.ref = Database.database().reference().child("users").child(uid!);
        
        imgProfile.asCircle()
        
        self.collectionView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getFriendsPendent(userID: self.uid)
        
        self.ref?.observe(.value, with: { (snapshot) in
            if let value = snapshot.value {
                self.user = UserModel.init(value:value as! [String : NSObject])
                self.inputData()
            }
        })
        
        getFriends()
    }
    
    func getFriends() {
        self.userBusiness.getFriendsCollection(self.uid) { (friend, error) in
            guard error == nil else {
                self.alertMensagem(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                return
            }
            self.friends = friend!
            self.collectionView.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.ref?.removeAllObservers()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func inputData() {
        
        if let url = user?.cover {
            imgCover.kf.setImage(with: URL(string:url)!)
        }
        
        if let url = user?.photo {
            imgProfile.kf.setImage(with: URL(string:url)!)
        }
        
        lblName.text = user?.name
        lblAddress.text = user?.address
        lblTotal.text = String(format: "Visitou %d lugares", (user?.travels)!)
    }
    
    @IBAction func btSair(_ sender: Any) {
        self.logOff()
        AppSession.sharedInstance.myFriends?.removeAll()
        AppSession.sharedInstance.friendsToFriends.removeAll()
        self.ref?.removeAllObservers()
    }

    @IBAction func btnEdit(_ sender: Any) {
        self.performSegue(withIdentifier: "sgDadosPessoais", sender: self.user)
    }
    
    @IBAction func btnAddFriend(_ sender: Any) {
        self.performSegue(withIdentifier: "segueAddFriends", sender: nil)
    }
    
    @IBAction func btnAddTrip(_ sender: Any) {
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "sgDadosPessoais" {
            
            let destVC = segue.destination as! UINavigationController
            let targeController = destVC.topViewController as! RegisterViewController
            targeController.userData = sender as! UserModel!
            targeController.isEditProfile = true
            
        }else if segue.identifier == "segueFriend" {
            
            let story = UIStoryboard(name: "Profile", bundle: nil)
            let vc    = story.instantiateViewController(withIdentifier: "FriendProfile") as! ProfileFriendViewController
            vc.uid  = sender as! String
            let nav   = UINavigationController(rootViewController: vc)
            nav.navigationBar.isTranslucent = true
            nav.navigationBar.barTintColor = UIColor.colorNavibar()
            nav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
            self.present(nav, animated: true, completion: nil)
            
            
        }
    }
}
extension MyProfileViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard self.friends.count > 0 else {
            return 0
        }
        return self.friends.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellFriends", for: indexPath as IndexPath) as! PhotoCollectionViewCell
        cell.inputUser(self.friends[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.allowsMultipleSelection = false
        collectionView.allowsSelection = true
        print("ITEM: \(indexPath.item)")
        self.performSegue(withIdentifier: "segueFriend", sender: self.friends[indexPath.item].uid)
    }
}
