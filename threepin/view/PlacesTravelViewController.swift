//
//  PlacesTravelViewController.swift
//  threepin
//
//  Created by Anderson Silva on 29/06/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit

class PlacesTravelViewController: BaseViewController {

    var idTravel:String!
    var titulo:String!
    var placeBusiness: PlaceBusiness!
    var objPlaces = [Place]()
    var total : Int = 0
    
    @IBOutlet weak var tbView: UITableView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.addBackButton()
        getPlaces()
        self.tbView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        addNew()
        self.title = self.titulo
        placeBusiness = PlaceBusiness.init()
        self.tbView.delegate = self
        
        let placeNIB = UINib(nibName: "PlaceTravelTableViewCell", bundle: nil)
        self.tbView.register(placeNIB, forCellReuseIdentifier: "cellPlaceTravel")
        self.tbView.tableFooterView = UIView()
        self.view.addSubview(self.tbView)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        placeBusiness.removeObservePlace()
    }
    
    func getPlaces() {
        self.showLoading()
        self.placeBusiness.getPlacesTravel(self.idTravel) { (places, error) in
            
            guard places != nil else {
                self.hideLoading()
                self.alertMensagem(title: "Ops!", msg: "Tente novamente mais tarde.", btn: "OK")
                return
            }
            
            self.objPlaces = places!
            self.total = (places?.count)!
            self.tbView.reloadData()
            self.hideLoading()
        }
        
    }
    
    func addNew() {
        
        let barButton = UIBarButtonItem(title: "Novo", style: .done, target: self, action: #selector(self.newButton))
        barButton.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = barButton
        
    }
    
    func newButton() {
        self.performSegue(withIdentifier: "sgNewPlace", sender: self.idTravel)
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sgNewPlace" {
            let vc = segue.destination as! PlaceAddViewController
            vc.idTravel = sender as! String
        }
    }
 

}
extension PlacesTravelViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return total
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tbView.dequeueReusableCell(withIdentifier: "cellPlaceTravel", for: indexPath) as! PlaceTravelTableViewCell
        cell.setup(place: self.objPlaces[indexPath.row])
        return cell
    }
    
}
