//
//  FollowPeopleViewController.swift
//  threepin
//
//  Created by Anderson Silva on 30/05/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Kingfisher

class FollowPeopleViewController: BaseViewController {

    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var segControl: UISegmentedControl!
    
    var uid:String!
    var userAll = [UserModel]()
    var friendAll = [Friends]()
    var total:Int = 0
    var objUser:UserModel?
    
    override func viewWillAppear(_ animated: Bool) {
        self.getFriendsPendent(userID: self.uid)
        self.addBackButton()
        self.getOthersUser()
        self.title = "Pessoas"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.userBusiness = UserBusiness.init()
        self.uid = self.userBusiness?.repository.getUid()
        
        self.userBusiness.getCurrentUser { (user, success, error) in
            guard user != nil else {
                return
            }
            self.objUser = user
        }
        
        // Do any additional setup after loading the view.
        self.tbView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.userBusiness.removeObserveUser()
    }
    
    func getOthersUser() {
        
        self.userBusiness.getOthersUser(self.uid) { (result, error) in
            guard error == nil else {
                return
            }
            
            self.userAll = result!
            self.total = self.userAll.count
            self.tbView.reloadData()
        }
    }
    
    func getFriendsPendent(){
        
        self.userBusiness.getFriendsPendent(self.uid) { (friend, error) in
            guard error == nil else {
                return
            }
            
            self.friendAll = friend!
            self.total = self.friendAll.count
            self.tbView.reloadData()
        }
        
    }
    
    func acceptFriend(indexPath:IndexPath) -> Bool{
        
        var value:Bool = true
        
        self.userBusiness.acceptFriendMe(self.friendAll[indexPath.row]) { (success, error) in
            if !success! {
                value = false
            }
        }
        
        self.userBusiness.acceptMeFriend(self.friendAll[indexPath.row], objUser: self.objUser!, completion: { (success, error) in
            if !success! {
                value = false
            }
        })
        return value
    }
    
    func removeFriend(indexPath:IndexPath) -> Bool {
        var value : Bool = true
        
        self.userBusiness.removePeople(uid: self.uid, idFriend: self.friendAll[indexPath.row].uid!) { (success, error) in
            if !success! {
                value = false
            }
        }
        return value
    }

    @IBAction func segControlAdd(_ sender: Any) {
        if self.segControl.selectedSegmentIndex == 0 {
            self.title = "Pessoas"
            self.tbView.allowsSelection = true
            self.getOthersUser()
        }else{
            self.title = "Solicitações de amizade"
            self.tbView.allowsSelection = false
            self.getFriendsPendent()
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "segueListPeopleAdd" {
            
            if let index = self.tbView.indexPathForSelectedRow {
                
                let story = UIStoryboard(name: "Profile", bundle: nil)
                let vc    = story.instantiateViewController(withIdentifier: "FriendProfile") as! ProfileFriendViewController
                vc.uid    = self.userAll[index.row].uid
                vc.addUser = true
                let nav   = UINavigationController(rootViewController: vc)
                nav.navigationBar.isTranslucent = true
                nav.navigationBar.barTintColor = UIColor.colorNavibar()
                nav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
                self.present(nav, animated: true, completion: nil)
                
                
            }
        }
    }
 

}
extension FollowPeopleViewController : UITableViewDelegate, UITableViewDataSource {        
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.total
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellPessoas", for: indexPath) as! PeopleFollowTableViewCell
        
        if self.segControl.selectedSegmentIndex == 0 {
            cell.inputUser(self.userAll[indexPath.row])
        }else{
            cell.inputFriend(self.friendAll[indexPath.row])
        }
        
        return cell
        //segueListPeopleAdd
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if self.segControl.selectedSegmentIndex == 0 {
            return false
        }else{
            return true
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        var result : [UITableViewRowAction]?
        
        if self.segControl.selectedSegmentIndex == 1 {
            
            //Remove
            let removeAction = UITableViewRowAction(style: .default, title: "Recusar", handler: { (action, indexPath) in
                
                self.userBusiness.deleteFriend(self.friendAll[indexPath.row].uid!, completion: { (success, error) in
                    if success! {
                        self.friendAll.remove(at: indexPath.row)
                    }else{
                        self.alertMensagem(title: "Ops!", msg: "Tente novamente", btn: "OK")
                    }
                    self.total = self.friendAll.count
                    self.tbView.deleteRows(at: [indexPath], with: .automatic)
                    if self.total > 0 {
                        self.tabBarController?.tabBar.items?[1].badgeValue = "\(self.total)"
                    }else{
                        self.tabBarController?.tabBar.items?[1].badgeValue = nil
                    }
                })
                self.userBusiness.removeObserveUser()
            })
            removeAction.backgroundColor = UIColor.red
            
            //Accept
            let acceptAction = UITableViewRowAction(style: .default, title: "Aceitar", handler: { (action, indexPath) in
                print("ACEITO2: \(String(describing: self.friendAll[indexPath.row].name))")
                
                if self.acceptFriend(indexPath: indexPath) {
                    self.friendAll.remove(at: indexPath.row)
                    self.total = self.friendAll.count
                    self.tbView.deleteRows(at: [indexPath], with: .automatic)
                    if self.total > 0 {
                        self.tabBarController?.tabBar.items?[1].badgeValue = "\(self.total)"
                    }else{
                        self.tabBarController?.tabBar.items?[1].badgeValue = nil
                    }
                }else{
                    self.alertMensagem(title: "Ops!", msg: "Tente novamente", btn: "OK")
                }
                self.userBusiness.removeObserveUser()
                                
            })
            acceptAction.backgroundColor = UIColor.colorGreen()
            result = [removeAction,acceptAction]
        }
        return result
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.performSegue(withIdentifier: "segueListPeopleAdd", sender: self.userAll[indexPath.row].uid)
//    }
}
