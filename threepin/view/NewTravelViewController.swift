//
//  NewTravelViewController.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 27/01/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit
import MobileCoreServices
import InputMask
import Kingfisher

private let reuseIdentifier = "item_photo"

class NewTravelViewController: BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, MaskedTextFieldDelegateListener, UICollectionViewDelegate, UICollectionViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{

    @IBOutlet weak var btnPermissao: UIButton!
    @IBOutlet weak var txPermissao: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var ImgViewCover: UIImageView!
    @IBOutlet weak var lblDays: UILabel!
    @IBOutlet weak var txtDestiny: UITextField!
    @IBOutlet weak var txtEndDate: UITextField!
    @IBOutlet weak var txtStartDate: UITextField!
    weak var currentTextField: UITextField!
    
    @IBOutlet weak var imgView4: UIImageView!
    @IBOutlet weak var imgView3: UIImageView!
    @IBOutlet weak var imgView2: UIImageView!
    @IBOutlet weak var imgView1: UIImageView!
    
//    @IBOutlet weak var lbPrivate: UILabel!
//    @IBOutlet weak var swPrivate: UISwitch!
    @IBOutlet weak var collectionView: UICollectionView!
    var maskedDate: MaskedTextFieldDelegate!
    
    var travelBusiness: TravelBusiness!
    var travel: Travel?
    var isCover:Bool = false
    var photos = [Photo]()
    
    let pickerView = UIPickerView()
    var permissoesData = Permissoes.allValues
    var currentData = ["PÚBLICA"]
    
    override func viewWillAppear(_ animated: Bool) {
        self.addBackButton()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        travelBusiness = TravelBusiness.init()
        
        maskedDate = MaskedTextFieldDelegate(format: "[00]/[00]/[0000]")
        maskedDate.listener = self
        self.pickerView.delegate = self
        self.txPermissao.delegate = self
        txtStartDate.delegate = maskedDate
        txtEndDate.delegate = maskedDate
        
        if travel != nil {
            inputData()
            
        }else {
            travel = Travel.init()
        }
    }
    
    private func inputData() {
        
        let startDate = Date.init(milliseconds: (travel?.startDate)!).dateString(format:"dd/MM/YYYY")
        let endDate = Date.init(milliseconds: (travel?.endDate)!).dateString(format:"dd/MM/YYYY")
        
        txtStartDate.text = startDate
        txtEndDate.text = endDate
        txtName.text = travel?.name
        txtDestiny.text = travel?.destiny
        txPermissao.text = travel?.permissao
        
        if let url = travel?.cover {
            self.ImgViewCover.kf.setImage(with: URL(string:url)!)
        }
        
        travelBusiness.observePhotosWithTravelID((travel?.id)!) { (photos, error) in
            guard error == nil else {
                return
            }
            
            self.photos = photos!
            self.collectionView.reloadData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        travelBusiness.removeObserve()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

//    @IBAction func back(_ sender: UIBarButtonItem?) {
//        _ = navigationController?.popViewController(animated: true)
//    }
    @IBAction func btnPermissao(_ sender: Any) {
        self.currentTextField = self.txPermissao
        self.txPermissao.inputView = self.pickerView
        self.currentData = self.permissoesData
        self.pickerView.reloadAllComponents()
        self.txPermissao.becomeFirstResponder()
    }
    
    @IBAction func saveTravel(_ sender: Any) {
        travel?.name = txtName.text!
        travel?.destiny = txtDestiny.text!
        travel?.permissao = txPermissao.text!
    
        let startDate = DateFormatter.dateFormatter().date(from: txtStartDate.text!)
        travel?.startDate = startDate?.millisecondsSince1970()
        
        let endDate = DateFormatter.dateFormatter().date(from: txtEndDate.text!)
        travel?.endDate = endDate?.millisecondsSince1970()
        self.showLoading()
        travelBusiness.create(travel!) { (result, error) in
            guard error == nil else {
                return self.alertMensagem(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
            }
            
            if self.photos.count > 0 {
                self.savePhotos((result?.id!)!)
            }
            else {
                self.hideLoading()
                self.backButton()
            }
        }
    }
    
    @IBAction func getCover(_ sender: Any) {
        isCover = true
        showActionSheetMaps(title: "", msg: "")
    }
    
    func openImagePicker() -> Void {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = [kUTTypeImage as String]
        imagePicker.allowsEditing = true
        
        present(imagePicker, animated: true, completion: nil)

    }
    
    func showActionSheetMaps(title:String, msg:String) {
        
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .actionSheet)
        
        let cancel = UIAlertAction(title: "Cancelar", style: .cancel) { (action) in
            
        }
        
        let maps = UIAlertAction(title: "Escolher Foto", style: .default) { (action) in
            self.openImagePicker()
        }
        
        let waze = UIAlertAction(title: "Buscar na Web", style: .default) { (action) in
//            self.performSegue(withIdentifier: "sgBuscarImg", sender: nil)
            
            let story = UIStoryboard(name: "Travel", bundle: nil)
            let vc    = story.instantiateViewController(withIdentifier: "buscarImagensViewController") as! BuscarImagensViewController
            vc.delegate = self
            let nav   = UINavigationController(rootViewController: vc)
            nav.navigationBar.isTranslucent = false
            self.present(nav, animated: true, completion: nil)
            
            
        }
        
        alertController.addAction(cancel)
        alertController.addAction(maps)
        alertController.addAction(waze)
        present(alertController, animated: true, completion: nil)
    }
    
    func savePhotos(_ key:String) {
        
        travelBusiness.savePhotos(photos: photos, travel:key) { (success, error) in
            guard success == true else {
                //TODO: tratar error
                return self.alertMensagem(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
            }
            
            self.backButton()
        }
    }
    
    /*@IBAction func swPrivate(_ sender: Any) {
        if self.swPrivate.isOn {
            self.lbPrivate.text = "Privado"
            self.travel?.isPrivate = true
        }else{
            self.lbPrivate.text = "Público"
            self.travel?.isPrivate = false
        }
    }*/
    
    func disableView(enable:Bool) {
        self.view.isUserInteractionEnabled = enable
        self.navigationItem.leftBarButtonItem?.isEnabled = enable
        self.navigationItem.rightBarButtonItem?.isEnabled = enable
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    /*override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "sgBuscarImg" {
            
            let vc:BuscarImagensViewController = segue.destination as! BuscarImagensViewController
            vc.delegate = self
            
        }
        
    }*/
 
    
    // MARK: - UIPickerDelegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.currentData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.currentData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.currentTextField.text = self.currentData[row]
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        var imageSelected:UIImage?
        
        if let editImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            imageSelected = editImage
            
        }else if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            imageSelected = pickedImage
        }
        
        
        if let image = imageSelected {
            
            var photo:Photo!
            
            if isCover {
                self.ImgViewCover.image = image
            }
            else {
                photo = Photo.init()
                photo.image = image
                self.photos.append(photo)
                self.collectionView.reloadData()
            }
            self.showLoading()
            self.disableView(enable: false)
            travelBusiness.uploadPhoto(image, completion: { (url, error) in
                
                if error == nil {
                    if self.isCover {
                        self.travel?.cover = url!
                        self.hideLoading()
                        self.disableView(enable: true)
                    }else{
                        photo?.url = url
                        self.hideLoading()
                        self.disableView(enable: true)
                    }
                }else{
                    self.alertMensagem(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                    self.hideLoading()
                    self.disableView(enable: true)
                }
                
                
                /*guard error == nil else {
                    // TODO: tratar error
                    return
                }
                
                if(self.isCover) {
                    self.travel?.cover = url!
                }
                else {
                    photo?.url = url
                }*/
                
            })
        }

        dismiss(animated: true, completion: nil)
        
    }
    
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func addImageInTravel(_ sender: Any) {
        isCover = false
        openImagePicker()
    }
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PhotoCollectionViewCell
        let photo = photos[indexPath.row]
        
        cell.inputData(photo)
        
        return cell
    }

}
extension NewTravelViewController : BuscarImagemDelegate {
    
    func getImage(link: String) {
        self.ImgViewCover.kf.setImage(with: URL(string: link))
        let foto = Photo.init()
        foto.image = self.ImgViewCover.image
//        self.photos.append(foto)
        
        self.showLoading()
        self.disableView(enable: false)
        travelBusiness.uploadPhoto(self.ImgViewCover.image!, completion: { (url, error) in
            
            if error == nil {
                
                self.travel?.cover = url!
                foto.url = url!
                self.photos.append(foto)
                self.hideLoading()
                self.disableView(enable: true)
                
                /*if self.isCover {
                    self.travel?.cover = url!
                    self.hideLoading()
                    self.disableView(enable: true)
                }else{
                    foto.url = url
                    self.hideLoading()
                    self.disableView(enable: true)
                }*/
            }else{
                self.alertMensagem(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                self.hideLoading()
                self.disableView(enable: true)
            }
            
        })
        
        
    }
    
}
