//
//  TermOfUseViewController.swift
//  threepin
//
//  Created by Râmede Bento on 17/10/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

import Foundation
import UIKit

class TermOfUseViewController: UIViewController {

    var termOfUseBusiness: TermOfUseBusiness!
    var placeBusiness: PlaceBusiness!
    
    @IBOutlet var tapClose: UITapGestureRecognizer!
    @IBOutlet weak var lblTermOfUseName: UILabel!
    @IBOutlet weak var lblTermOfUseContent: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        termOfUseBusiness = TermOfUseBusiness.init()
        termOfUseBusiness.getLastTermOfUse({ (term, message) in
            self.lblTermOfUseName.text = term.name
            self.lblTermOfUseContent.text = term.content
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        termOfUseBusiness.removeObservePlace()
    }
        
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func displayGestureForTapRecognizer(_ recognizer: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
       
    //close_termo
}
