//
//  PlacesViewController.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 12/19/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

import UIKit
import GooglePlaces
import CoreLocation

class PlacesViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var placeBusiness: PlaceBusiness!
    var objPlaces =  [Place]()
    var total : Int = 0
    var searchActive : Bool = false
    var resultsViewController: GMSAutocompleteResultsViewController?
    var fetcher: GMSAutocompleteFetcher?
    var searchController: UISearchController?
    var placeID:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        self.tableView.delegate = self

        placeBusiness = PlaceBusiness.init();
        placesClient = GMSPlacesClient.shared()
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        // Put the search bar in the navigation bar.
        searchController?.searchBar.sizeToFit()
        navigationItem.titleView = searchController?.searchBar
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        definesPresentationContext = true
        
        // Prevent the navigation bar from being hidden when searching.
        searchController?.hidesNavigationBarDuringPresentation = false
        
        
        loadXib()
        getPlaces()
        
    }
    
    func completeFetcher(currentLocation: CLLocation) {
        
        let neBoundsCorner = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        let swBoundsCorner = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        
        let filter = GMSAutocompleteFilter()
        filter.type = .establishment
        let bounds = GMSCoordinateBounds(coordinate: neBoundsCorner, coordinate: swBoundsCorner)
        resultsViewController?.autocompleteBounds = bounds
        resultsViewController?.autocompleteFilter = filter
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        LocationService.sharedInstance.delegate = self
        LocationService.sharedInstance.startUpdatingLocation()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        LocationService.sharedInstance.stopLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadXib() {
        let placeNIB = UINib(nibName: "PlaceTravelTableViewCell", bundle: nil)
        self.tableView.register(placeNIB, forCellReuseIdentifier: "cellPlaceTravel")
        self.tableView.tableFooterView = UIView()
        self.view.addSubview(self.tableView)
    }
    
    func getPlaces() {
        self.showLoading()
        placeBusiness.getPlaces { (result, error) in
            guard result != nil else {
                self.hideLoading()
                self.alertMensagem(title: "Ops!", msg: "Tente novamente mais tarde.", btn: "OK")
                return
            }
            
            self.objPlaces = result!
            self.total  = (result?.count)!
            self.tableView.reloadData()
            self.hideLoading()
        }
    }
    
    func getPhotos(place:GMSPlace) {
        
        self.placesClient.lookUpPhotos(forPlaceID: place.placeID) { (placePhotos, error) in
            guard error == nil else {
                print("ERROR: \(error.debugDescription)")
                return
            }
            
            let firstPhoto = placePhotos?.results.first
            self.loadImageForMetadata(photoMetadata: firstPhoto!, place: place)
        }
        
    }
    
    func loadImageForMetadata(photoMetadata: GMSPlacePhotoMetadata, place:GMSPlace) {
        
        self.placesClient.loadPlacePhoto(photoMetadata) { (image, error) in
            guard error == nil else {
                print("ERROR: \(error.debugDescription)")
                return
            }
            
            let places = Place.init()
            places.name = place.name
            places.address = place.formattedAddress
            places.countLikes = 0
            places.countComments = 0
            places.countTrips = 0
            places.countStars = 0
            places.imagem = image
            places.url = place.website
            
            self.objPlaces.insert(places, at: 0)
            self.total = self.objPlaces.count
            self.tableView.reloadData()
            
        }
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "sgPlacesTravelAdd" {
            let vc:AddFriendTravelViewController = segue.destination as! AddFriendTravelViewController
            vc.objPlaces = sender as? Place
        }
        
        if segue.identifier == "sgBrowser" {
            
            let story = UIStoryboard(name: "Places", bundle: nil)
            let vc    = story.instantiateViewController(withIdentifier: "openBrowserViewController") as! OpenBrowserViewController
            vc.place    = sender as? Place
            let nav   = UINavigationController(rootViewController: vc)
            nav.navigationBar.isTranslucent = true
            nav.navigationBar.barTintColor = UIColor.colorNavibar()
            nav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
            self.present(nav, animated: true, completion: nil)
            
        }
        
    }
    

}
extension PlacesViewController : UITableViewDelegate, UITableViewDataSource, PlaceTravelTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.total
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cellPlaceTravel", for: indexPath) as! PlaceTravelTableViewCell
        cell.delegate = self
        cell.setup(place: self.objPlaces[indexPath.row], btn: true)
        return cell
    }
    
    func btnTrip(in cell : PlaceTravelTableViewCell) {
        if let indexPath = self.tableView.indexPath(for: cell) {
            self.performSegue(withIdentifier: "sgPlacesTravelAdd", sender: self.objPlaces[indexPath.row])
        }
    }
    
    func btnBrowser(in cell: PlaceTravelTableViewCell) {
        if let indexPath = self.tableView.indexPath(for: cell) {
            if self.objPlaces[indexPath.row].url != nil {
                self.performSegue(withIdentifier: "sgBrowser", sender: self.objPlaces[indexPath.row])
            }
        }
    }
    
}

//MARK: - GooglePlacesDelegate
extension PlacesViewController: GMSAutocompleteResultsViewControllerDelegate {
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        
        // Do something with the selected place.
        self.getPhotos(place: place)
        
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController,
                           didFailAutocompleteWithError error: Error){
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
 
extension PlacesViewController : LocationServiceDelegate {
    func tracingLocation(currentLocation: CLLocation) {
        self.completeFetcher(currentLocation: currentLocation)
    }
    
    func tracingLocationFailWithError(error: NSError) {
        print("Error: \(error.localizedDescription)")
    }
}
