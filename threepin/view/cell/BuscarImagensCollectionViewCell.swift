//
//  BuscarImagensCollectionViewCell.swift
//  threepin
//
//  Created by Anderson Silva on 28/11/2017.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit
import Kingfisher

class BuscarImagensCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var img: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(item:GoogleSearchItems) {
        
        self.img.kf.setImage(with: URL(string: item.link!))
        
    }

}
