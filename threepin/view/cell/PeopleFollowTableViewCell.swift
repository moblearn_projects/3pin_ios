//
//  PeopleFollowTableViewCell.swift
//  threepin
//
//  Created by Anderson Silva on 30/05/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit

class PeopleFollowTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imgPeople: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    var photo:Photo?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func inputUser(_ user:UserModel) {
        
        guard user.photo != nil else {
            self.imgPeople.image = photo?.image
            return
        }
        
        self.imgPeople.kf.setImage(with: URL(string: user.photo!))
        self.imgPeople.asCircle()
        self.name.text = user.name
        
    }
    
    func inputFriend(_ friend:Friends) {
        
        guard friend.photo != nil else {
            self.imgPeople.image = photo?.image
            return
        }
        
        self.imgPeople.kf.setImage(with: URL(string: friend.photo!))
        self.imgPeople.asCircle()
        self.name.text = friend.name
        
    }

}
