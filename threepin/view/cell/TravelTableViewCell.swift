//
//  TravelTableViewCell.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 31/01/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit
import Kingfisher

class TravelTableViewCell: UITableViewCell {

    @IBOutlet weak var cover: UIImageView!
    @IBOutlet weak var lblDateAndName: UILabel!
    @IBOutlet weak var imgLikes: UIImageView!
    @IBOutlet weak var imgLock: UIImageView!
    @IBOutlet weak var lblLikes: UILabel!
    
    var business: TravelBusiness!
    var travel: Travel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        business = TravelBusiness.init()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapLike(_:)))
        self.imgLikes.addGestureRecognizer(tapGesture)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setData(_ travel: Travel) {
        self.travel = travel
        
        if let url = travel.cover {
            self.cover.kf.setImage(with: URL(string:url)!)
        }
       
        if travel.isPrivate {
            imgLock.image = Image.init(imageLiteralResourceName: getIcLock())
        }else {
            imgLock.image = Image.init(imageLiteralResourceName: getIcUnlock())
        }
        
        lblLikes.text = String.init(format: "%d curtidas", travel.likes)
        
        let dateStart = Date.init(milliseconds: travel.startDate!).dateString(format: "MM/yyyy")
        lblDateAndName.text = String(format: "%@\n%@", dateStart, travel.name!)
        
        if AppSession.sharedInstance.isLikeTravel(travel.id!) {
            imgLikes.image = imgLikes.image!.withRenderingMode(.alwaysTemplate)
            imgLikes.tintColor = UIColor.red
        }
        
    }
    
    private func getIcLock() -> String{
        guard cover != nil else {
            return "ic_lock_grey"
        }
        
        return "ic_lock_white"
    }
    
    
    private func getIcUnlock() -> String{
        guard cover != nil else {
            return "ic_unlock_grey"
        }
        
        return "ic_unlock_white"
    }
    
    func tapLike(_ sender: UITapGestureRecognizer) {
        
        if AppSession.sharedInstance.isLikeTravel(travel.id!) {
            business.unLike(travel)
        }
        else {
            business.like(travel) 
        }
        
        lblLikes.text = String.init(format: "%d curtidas", travel.likes)
    }
    

}
