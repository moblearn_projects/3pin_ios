//
//  PhotoCollectionViewCell.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 10/02/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit
import Kingfisher

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var imgSelecionado: UIImageView!
    
    var photo:Photo?
    
    func inputData(_ photo:Photo) {
        self.photo = photo
        self.imgSelecionado.isHidden = true
        guard self.photo?.image == nil else {
            imgPhoto.image = photo.image
            return
        }
        
        imgPhoto.kf.setImage(with: URL.init(string: photo.url))
    }
    
    func inputUser(_ user:Friends) {        
        
        guard user.photo != nil else {
            imgPhoto.image = photo?.image
            return
        }
        
        imgPhoto.kf.setImage(with: URL(string: user.photo!))
        imgPhoto.asCircle()
    }
    
    func imgSelecionadoExibir() {
        self.imgSelecionado.isHidden = false        
    }
    
    func imgSelecionadoRemover() {
        self.imgSelecionado.isHidden = true
    }
}
