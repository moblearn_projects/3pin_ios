//
//  PlaceTableViewCell.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 12/19/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

import UIKit

class PlaceTableViewCell: UITableViewCell {

    @IBOutlet weak var imagePlace: UIImageView!
    @IBOutlet weak var categoryPlace: UILabel!
    @IBOutlet weak var namePlace: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
