//
//  PlaceTravelTableViewCell.swift
//  threepin
//
//  Created by Anderson Silva on 29/06/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit

protocol PlaceTravelTableViewCellDelegate : class {
    func btnTrip(in cell : PlaceTravelTableViewCell)
    func btnBrowser(in cell : PlaceTravelTableViewCell)
}

class PlaceTravelTableViewCell: UITableViewCell {

    
    @IBOutlet weak var imgPlace: UIImageView!
    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var lbAddress: UILabel!
    @IBOutlet weak var lbLike: UILabel!
    @IBOutlet weak var lbRoteiros: UILabel!
    @IBOutlet weak var btnTrip: UIButton!
    @IBOutlet weak var btnBrowser: UIButton!
    
    weak var delegate : PlaceTravelTableViewCellDelegate?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(place:Place,btn:Bool=false) {
        
        if place.photo != nil {
            self.imgPlace.kf.setImage(with: URL(string:place.photo!))
        }else{
            self.imgPlace.image = place.imagem
        }
        
        self.nome.text = place.name!
        self.lbAddress.text = place.address!
        self.lbLike.text = "\(String(describing: place.countLikes!))"
        self.lbRoteiros.text = "\(String(describing: place.countTrips!)) Roteiros"
        
        if !btn {
            self.btnTrip.isHidden = true
        }
        
        
    }
    
    @IBAction func btnBrowser(_ sender: Any) {
        delegate?.btnBrowser(in: self)
    }
    
    @IBAction func btnTrip(_ sender: Any) {
        delegate?.btnTrip(in: self)
    }
}
