//
//  PeopleTravelTableViewCell.swift
//  threepin
//
//  Created by Anderson Silva on 18/06/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit

protocol PeopleTravelTableViewCellDelegate : class {
    func btnRemove(in cell: PeopleTravelTableViewCell)
}

class PeopleTravelTableViewCell: UITableViewCell {

    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var btnRemove: UIButton!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbAddress: UILabel!
    @IBOutlet weak var lbNPlaces: UILabel!
    
    weak var delegate : PeopleTravelTableViewCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(user:UserModel) {
        
        self.imgPhoto.kf.setImage(with: URL(string: user.photo!))
        self.imgPhoto.asCircle()
        self.lbName.text = user.name
        self.lbAddress.text = user.address
        self.lbNPlaces.text = "Visitou \(user.travels) lugares"
    }
    
    @IBAction func removeUser(_ sender: Any) {
        delegate?.btnRemove(in: self)
    }
    
}
