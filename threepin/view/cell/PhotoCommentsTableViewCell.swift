//
//  PhotoCommentsTableViewCell.swift
//  threepin
//
//  Created by Anderson Silva on 05/11/2017.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit

class PhotoCommentsTableViewCell: UITableViewCell {

    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var comentario: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(comentario:PhotoComment) {
        
        self.nome.text = comentario.nameUser
        self.comentario.text = comentario.comments
        
    }
    
}
