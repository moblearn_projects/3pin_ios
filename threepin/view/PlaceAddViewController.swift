//
//  PlaceAddViewController.swift
//  threepin
//
//  Created by Anderson Silva on 29/06/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit
import CoreLocation

class PlaceAddViewController: BaseViewController, UINavigationControllerDelegate {

    @IBOutlet weak var txName: UITextField!
    @IBOutlet weak var txAddress: UITextField!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btSave: UIBarButtonItem!
    
    var place:Place!
    var photo:URL!
    var placeBusiness:PlaceBusiness?
    var idTravel:String!
    var currentLocation : CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBackButton()
        LocationService.sharedInstance.delegate = self
        hideKeyboardWhenTappedAround()
        self.placeBusiness = PlaceBusiness.init()
        self.place = Place.init()
        
        
        if place != nil {
            inputData()
            self.txName.text = place?.name
            self.txAddress.text = place?.address
        }
        
        getImageTap()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        LocationService.sharedInstance.startUpdatingLocation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        placeBusiness?.removeObservePlace()
        LocationService.sharedInstance.stopLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btSave(_ sender: Any) {
        savePlace()
    }
    
    private func inputData() {
        if let urlPhoto = place?.photo {
            imgView.kf.setImage(with: URL(string: urlPhoto)!)
        }
    }
    
    func savePlace() {
        
        if self.txAddress.text == "" || self.txName.text == "" {
            self.alertMensagem(title: "Campos Obrigatórios!", msg: "Favor preencher os campos obrigatórios.", btn: "OK")
        }else{
            place!.name = self.txName.text!
            place!.address = self.txAddress.text!
            place!.countLikes = 0
            place!.countComments = 0
            place!.countTrips = 0
            place!.countStars = 0
            place!.uidTravel = idTravel
            place!.latitude  = self.currentLocation?.coordinate.latitude
            place!.longitude = self.currentLocation?.coordinate.longitude
            
            self.placeBusiness?.createPlace(place!, completion: { (success, error) in
                if (error == nil) {
                    self.navigationController?.popViewController(animated: true)
                }else{
                    self.alertMensagem(title: "Ops!", msg: error as! String, btn: "OK")
                }
            })
        }
        
    }
    
    func getImageTap() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.getImage(_:)))
        self.imgView.addGestureRecognizer(tap)
    }
    
    @objc private func getImage(_ sender: UITapGestureRecognizer) {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
//        imagePicker.mediaTypes = [kUTTypeImage as String]
        imagePicker.allowsEditing = true
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    func disableView(enable:Bool) {
        self.view.isUserInteractionEnabled = enable
        self.navigationItem.leftBarButtonItem?.isEnabled = enable
        self.navigationItem.rightBarButtonItem?.isEnabled = enable
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PlaceAddViewController : UIImagePickerControllerDelegate {
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        var imageSelected:UIImage?
        
        if let editImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            imageSelected = editImage
        }
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            imageSelected = pickedImage
        }
        
        
        if let image = imageSelected {
                
            imgView.image = image
            self.view.isUserInteractionEnabled = false
            self.disableView(enable: false)
            self.showLoading()
            self.placeBusiness?.uploadPhoto(image, completion: { (url, error) in
                guard error == nil else {
                    self.alertMensagem(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                    self.hideLoading()
                    self.disableView(enable: true)
                    return
                }
                self.place?.photo = url!
                self.hideLoading()
                self.disableView(enable: true)
            })
        }
        
        
        dismiss(animated: true, completion: nil)
        
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
}
extension PlaceAddViewController : LocationServiceDelegate {
    func tracingLocation(currentLocation: CLLocation) {
        self.currentLocation = currentLocation
    }
    
    func tracingLocationFailWithError(error: NSError) {
        print("Error: \(error.localizedDescription)")
    }
}
