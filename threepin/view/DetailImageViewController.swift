//
//  DetailImageViewController.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 11/02/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit
import Kingfisher
import FirebaseAuth

class DetailImageViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    var photo:Photo?
    var photoComment:PhotoComment?
    var placeBusiness: PlaceBusiness!
    var travelBusiness:TravelBusiness!
    var objComments : [PhotoComment]?
    var total = 0
    var urlImage:String?
    
    var uid:String!
    var nameUser:String!

    @IBOutlet weak var fieldComment: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imgPhoto: UIImageView!
    @IBOutlet weak var btnImagem: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        placeBusiness = PlaceBusiness.init()
        self.userBusiness = UserBusiness.init()
        travelBusiness = TravelBusiness.init()
        hideKeyboardWhenTappedAround()
        self.loadComments()
        
        self.uid = Auth.auth().currentUser?.uid
        self.nameUser = Auth.auth().currentUser?.displayName
        
//        self.uid = self.userBusiness?.repository.getUid()
        if photo?.uid != self.uid {
            self.navigationItem.rightBarButtonItem = nil
        }
        
        guard let url = photo?.url else {
            imgPhoto.image = photo?.image
            return
        }
        self.urlImage = url
        imgPhoto.kf.setImage(with: URL(string: url))

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(_ sender: UIBarButtonItem?) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendComment(_ sender: Any) {
        
        if !(self.fieldComment.text?.isEmpty)!{
        
            self.photoComment = PhotoComment()
            self.photoComment?.comments = self.fieldComment.text
            self.photoComment?.id = photo?.id
            self.photoComment?.travel = photo?.travel
            self.photoComment?.url = photo?.url
            self.photoComment?.uid = self.uid
            self.photoComment?.nameUser = self.nameUser
            
            self.placeBusiness.sendCommentPhoto(self.photoComment!) { (result, error) in
                if (error == nil) {
                    self.fieldComment.text = ""
                    self.loadComments()
                }else{
                    self.alertMensagem(title: "Ops!", msg: error as! String, btn: "OK")
                }
            }
            
        }else{
            self.alertMensagem(title: "Ops!", msg: "Campo Obrigatório!", btn: "OK")
        }
        
    }
    
    @IBAction func removeImage(_ sender: Any) {
        callRemoveImage()
    }
    
    func callRemoveImage() {
        
        let alertController = UIAlertController(title: "3Pin informa", message: "Você deseja excluir está foto?", preferredStyle: .actionSheet)
        
        let cancel = UIAlertAction(title: "Cancelar", style: .cancel) { (action) in
            
        }
        
        let excluir = UIAlertAction(title: "Excluir", style: .default) { (action) in
            self.travelBusiness.removePhotoTravel(photoID: (self.photo?.id)!, travelID: (self.photo?.travel)!, completion: { (msg, error) in
                guard error == nil else {
                    self.alertMsg(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                    return
                }
                self.navigationController?.popViewController(animated: true)
            })
        }
        
        alertController.addAction(cancel)
        excluir.setValue(UIColor.red, forKey: "titleTextColor")
        alertController.addAction(excluir)
        present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func abrirImagem(_ sender: Any) {
        self.performSegue(withIdentifier: "sgImagemFull", sender: nil)
    }
    
    func loadComments() {
        self.placeBusiness.getPhotosComments(self.photo?.id, completion: { (photoComments, error, success) in
            guard error == nil else {
                self.alertMensagem(title: "Ops!", msg: error as! String, btn: "OK")
                return
            }
            
            if success! {
                self.objComments = photoComments!
                self.total = photoComments!.count
            }
            self.tableView.reloadData()
        })
        loadXib()
    }
    
    func loadXib() {
        
        let itemXi = UINib(nibName: "PhotoCommentsTableViewCell", bundle: nil)
        self.tableView.register(itemXi, forCellReuseIdentifier: "commentCell")
        
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "sgImagemFull" {
            let vc : ImageFullViewController = segue.destination as! ImageFullViewController
            vc.url = self.urlImage!
        }
        
    }
 
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return total
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! PhotoCommentsTableViewCell
        cell.setup(comentario: self.objComments![indexPath.row])
        return cell
    }
    
}
