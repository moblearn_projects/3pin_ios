//
//  OpenBrowserViewController.swift
//  threepin
//
//  Created by Anderson Silva on 22/01/2018.
//  Copyright © 2018 Moblearn. All rights reserved.
//

import UIKit

class OpenBrowserViewController: BaseViewController {

    var place : Place?
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.addBackModal()
        self.title = self.place?.name
        self.webView.loadRequest(URLRequest(url: (place?.url!)!))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
