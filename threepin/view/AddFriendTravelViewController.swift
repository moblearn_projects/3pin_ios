//
//  AddFriendTravelViewController.swift
//  threepin
//
//  Created by Anderson Silva on 08/06/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit


class AddFriendTravelViewController: BaseViewController, UITextFieldDelegate {

    
    @IBOutlet weak var lbAddFriend: UILabel!
    @IBOutlet weak var viewAddFT: ViewBorderTextField!
    @IBOutlet weak var txAddFT: UITextField!
    @IBOutlet weak var btnAddFT: UIButton!
    @IBOutlet weak var btnAdicionar: UIButton!
    
    weak var currentTextField: UITextField!
    var travelBusiness: TravelBusiness!
    
    var uid:String!
    var idTravel:String?
    var uidFriend:String!
    let pickerView = UIPickerView()
    let dados = ["Vila Rica","São Paulo","Recife"]
    var dataTravels = [Travel]()
    var currentData = ["None"]
    var total = 0
    var objPlaces:Place?
    var placeBusiness:PlaceBusiness?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userBusiness = UserBusiness.init()
        self.uid = self.userBusiness?.repository.getUid()
        travelBusiness = TravelBusiness.init();
        
        if objPlaces != nil {
//            self.objPlaces = Place.init()
            self.placeBusiness = PlaceBusiness.init()
        }
        
        self.txAddFT.delegate = self
        self.pickerView.delegate = self

        // Do any additional setup after loading the view.
        self.addBlurArea(area: self.view, style: .dark)
        getTravels()

        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapDismiss(_:)))
        self.view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.userBusiness.removeObserveUser()
    }
    
    
    @IBAction func btnAddFT(_ sender: Any) {
        
        self.currentTextField = self.txAddFT
        self.txAddFT.inputView = self.pickerView
        self.currentData = self.dados
        self.pickerView.reloadAllComponents()
        self.txAddFT.becomeFirstResponder()
        
    }
    
    func addPlaces() {
        
        self.objPlaces?.uidTravel = self.idTravel
        self.placeBusiness?.createPlace(self.objPlaces!, completion: { (place, error) in
            guard error == nil else {
                self.alertMensagem(title: "Ops!", msg: "Ocorreu um error. Por favor, tente novamente.", btn: "OK")
                return
            }
            self.dismiss(animated: true, completion: nil)
        })
        
    }

    @IBAction func btnAdicionar(_ sender: Any) {
        if self.txAddFT.text != "" {
            if self.objPlaces == nil {
                self.addFriendTravel()
            }else{
                self.addPlaces()
            }
        }else{
            self.alertMensagem(title: "Ops!", msg: "Por favor, selecione uma viagem.", btn: "OK")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func addFriendTravel() {
        
        self.travelBusiness.createTravelUser(uid: self.uidFriend, travelID: self.idTravel!, isPrivate: false) { (success, error) in
            if success! {
                self.dismiss(animated: true, completion: nil)
            }else{
                self.alertMensagem(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
            }
        }
        
    }
    
    func getTravels() {
        self.travelBusiness.getTravelsByUId(self.uid, trip: true) { (travel, error) in
            guard travel != nil else {
                return self.alertMensagem(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
            }
            self.dataTravels = travel!
            self.total = self.dataTravels.count
        }
    }

}
extension AddFriendTravelViewController : UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.total
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if self.dataTravels.count > 0 {
            return self.dataTravels[row].name
        }else{
            return currentData[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if self.dataTravels.count > 0 {
            self.currentTextField.text = self.dataTravels[row].name
            self.idTravel = self.dataTravels[row].id
        }else{
            self.currentTextField.text = self.currentData[row]
        }
        
    }
}
