//
//  BuscarImagensViewController.swift
//  threepin
//
//  Created by Anderson Silva on 28/11/2017.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit
import Kingfisher

protocol BuscarImagemDelegate : class {
    func getImage(link:String)
}

class BuscarImagensViewController: BaseViewController {

    var itens:[GoogleSearchItems]?
    var total = 0
    var travelNegocios = TravelNegocios()
    
    weak var delegate : BuscarImagemDelegate?
    weak var imgView : UIImage?
    
    @IBOutlet weak var collection: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.searchBar.delegate = self
        self.collection.delegate = self
        self.collection.dataSource = self
        self.travelNegocios.baseViewInterface = self
        self.travelNegocios.interface = self
        self.searchBar.becomeFirstResponder()
        self.searchNavigation(searchBar: self.searchBar, placeholder: "BUSCAR CIDADES", barTintColor: UIColor.white, view: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadXib() {
        
        let collectionNIB = UINib(nibName: "BuscarImagensCollectionViewCell", bundle: nil)
        self.collection.register(collectionNIB, forCellWithReuseIdentifier: "buscarImgCell")
        self.view.addSubview(self.collection)
        self.collection.reloadData()
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BuscarImagensViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.total
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collection.dequeueReusableCell(withReuseIdentifier: "buscarImgCell", for: indexPath) as! BuscarImagensCollectionViewCell
        cell.setup(item: self.itens![indexPath.row])
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.dismiss(animated: true) {
            self.delegate?.getImage(link: self.itens![indexPath.row].link!)
        }
        
    }
    
}
extension BuscarImagensViewController : UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        self.travelNegocios.getDataGoogle(q: searchBar.text!)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
}
extension BuscarImagensViewController : TravelInterface {
    
    func updateImages(total: Int, itens: [GoogleSearchItems]) {
        self.total = total
        self.itens = itens
        self.loadXib()
    }
    
}
