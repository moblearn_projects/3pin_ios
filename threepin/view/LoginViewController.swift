//
//  ViewController.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 10/4/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
    
    
    @IBOutlet weak var btFacebook: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.userBusiness = UserBusiness.init()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.btFacebook.isHidden = true
        self.userBusiness.getCurrentUser({ (user, isNewUser, error) in
            if user != nil {
                self.performSegue(withIdentifier: "segToUsersAndSocial", sender: nil)
            }else{
                self.btFacebook.isHidden = false
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "segToRegister"){
            
            let destVC = segue.destination as! UINavigationController
            let targeController = destVC.topViewController as! RegisterViewController
            targeController.userData = sender as! UserModel!
            
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .default
    }
    
   
    @IBAction func loginWithFacebook(_ sender: Any) {
        self.userBusiness.loginWithFacebook(self, completion: { (user, isNewUser) in
            if (isNewUser){
                self.performSegue(withIdentifier: "segToRegister", sender: user)
            } else {
                AppSession.sharedInstance.addCurrentUser(user!)
                self.userBusiness.getFriends(user!.uid!, completion: { (friends, error) in
                    guard friends != nil else {
                        return
                    }
                    AppSession.sharedInstance.myFriends = friends!
                    for friend in friends! {
                        self.userBusiness.getFriends(friend.uid!, completion: { (allFriends, error) in
                            guard allFriends != nil else {
                                return
                            }
                            for value in allFriends! {
                                if (friend.uid! != value.uid) && (user!.uid != value.uid){
                                    AppSession.sharedInstance.friendsToFriends.append(value)
                                }
                            }
                        })
                    }
                    self.performSegue(withIdentifier: "segToUsersAndSocial", sender: user)
                })
            }
        }) { (messageError) in
            self.alertMensagem(title: "Ops!", msg: messageError!, btn: "OK")
        }
    }
}

