//
//  RegisterViewController.swift
//  threepin
//
//  Created by Râmede Bento on 21/10/16.
//  Copyright © 2016 Moblearn. All rights reserved.
//

import UIKit
import DLRadioButton
import Kingfisher
import MobileCoreServices
import InputMask

class RegisterViewController: BaseViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, MaskedTextFieldDelegateListener{
    
    @IBOutlet weak var txtSchooling: UITextField!
    @IBOutlet weak var txtIncome: UITextField!
    @IBOutlet weak var txtCivilStatus: UITextField!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtBirthday: UITextField!
    @IBOutlet weak var txtJob: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    
    @IBOutlet weak var btnFemale: DLRadioButton!
    @IBOutlet weak var btnMale: DLRadioButton!
    
    @IBOutlet weak var imageViewCover: UIImageView!
    @IBOutlet weak var imageViewPhoto: UIImageView!
    
    weak var currentTextField: UITextField!
    
    var userData:UserModel!
    var isProfilePhoto:Bool = false
    var isEditProfile:Bool  = false
    
    var photo:URL!
    var cover:URL!
    
    var maskedDate: MaskedTextFieldDelegate!
    var maskedPhone: MaskedTextFieldDelegate!

    
    //Todo remove fixed strings
    let pickerView = UIPickerView()
    let schoolarityData = ["Fundamental","Médio","Superior Incompleto","Superior","Pós-Graduado"]
    let incomeData = ["Menos de R$2500","Entre R$ 2500,00 a R$ 3500,00","Entre R$ 4500,00 a R$ 5500,00", "Acima de R$ 5500,00"]
    let civilStatusData = ["Solteiro(a)","Casado(a)","Divorciado(a)","Viúvo(a)","Separado(a)"]
   
    var currentData = ["None"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        self.addBackModal()
        
        hideKeyboardWhenTappedAround()
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = UIColor(red: 245.0 / 255.0, green: 127.0 / 255.0, blue: 127.0 / 255.0, alpha: 0.9)
        
        self.userBusiness = UserBusiness.init()
        
        self.pickerView.delegate = self
        self.txtSchooling.delegate = self
        self.txtIncome.delegate = self
        self.txtCivilStatus.delegate = self
        
        maskedDate = MaskedTextFieldDelegate(format: "[00]/[00]/[0000]")
        maskedDate.listener = self
        self.txtBirthday.delegate = maskedDate
        
        maskedPhone = MaskedTextFieldDelegate(format: "([00]) [00000]-[0000]")
        maskedPhone.listener = self
        self.txtPhone.delegate = maskedPhone
        
        if(userData == nil) {
            userData = UserModel.init()
        }else{
            
             self.inputData()
             self.txtName.text = userData!.name
             self.txtAddress.text = userData!.address
             self.txtPhone.text = userData!.phone
             self.txtBirthday.text = userData!.dateOfBirth
             self.txtJob.text = userData!.occupation
             self.txtCivilStatus.text = userData!.maritalStatus
             self.txtSchooling.text = userData!.schooling
             self.txtJob.text = userData!.occupation
             self.txtIncome.text = userData!.income
            if userData!.gender == "Masculino" {
                self.setGender(self.btnMale)
            }else{
                self.setGender(self.btnFemale)
            }
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle{
        return .lightContent
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        statusBar.backgroundColor = UIColor.clear
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.currentData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.currentData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.currentTextField.text = self.currentData[row]
    }
    
    @IBAction func incomeTap(_ sender: AnyObject){
        self.currentTextField = self.txtIncome
        self.txtIncome.inputView = self.pickerView
        self.currentData = self.incomeData
        self.pickerView.reloadAllComponents()
        self.txtIncome.becomeFirstResponder()
    }

    @IBAction func schoolarityTap(_ sender: AnyObject){
        self.currentTextField = self.txtSchooling
        self.txtSchooling.inputView = self.pickerView
        self.currentData = self.schoolarityData
        self.pickerView.reloadAllComponents()
        self.txtSchooling.becomeFirstResponder()
    }

    @IBAction func civilStatusTap(_ sender: AnyObject){
        self.currentTextField = self.txtCivilStatus
        self.txtCivilStatus.inputView = self.pickerView
        self.currentData = self.civilStatusData
        self.pickerView.reloadAllComponents()
        self.txtCivilStatus.becomeFirstResponder()
    }
    
    @IBAction func setGender(_ sender: AnyObject){

        //todo remove fixed strings
        if (sender as! NSObject == btnMale){
            userData.gender = "Masculino"
        }
        else
        if (sender as! NSObject == btnFemale){
            userData.gender = "Feminino"
        }
        
    }
    
    @IBAction func userSave(_ sender: Any) {
        
        userData.name = self.txtName.text!
        userData.address = self.txtAddress.text!
        userData.phone = self.txtPhone.text!
        userData.dateOfBirth = self.txtBirthday.text!
        userData.occupation = self.txtJob.text!
        userData.maritalStatus = self.txtCivilStatus.text!
        userData.schooling = self.txtSchooling.text!
        userData.occupation = self.txtJob.text!
        userData.income = self.txtIncome.text!
        
        if !isEditProfile {
            self.userBusiness.createUser(user: userData) { (success, message) in
                if success {
                    self.performSegue(withIdentifier: "segToMain", sender: nil)
                }else{
                    self.alertMensagem(title: "Ops!", msg: message!, btn: "OK")
                }
            }
        }else{
            self.userBusiness.updateUser(user: userData, completion: { (success, message) in
                if success {
                    self.dismiss(animated: true, completion: nil)
                }else{
                    self.alertMensagem(title: "Ops!", msg: message! as! String, btn: "OK")
                }
            })
        }
    }
    

    @IBAction func getImageProfile(_ sender: Any) {
        isProfilePhoto = true
        getImage()
    }
    
    @IBAction func getImageBackground(_ sender: Any) {
        isProfilePhoto = false
        getImage()
    }
    
    private func inputData() {
        
        if let url = userData?.cover {
            imageViewCover.kf.setImage(with: URL(string: url)!)
        }
        
        if let urlPhoto = userData?.photo {
            imageViewPhoto.kf.setImage(with: URL(string: urlPhoto)!)
        }
    }
    
    func disableView(enable:Bool) {
        self.view.isUserInteractionEnabled = enable
        self.navigationItem.leftBarButtonItem?.isEnabled = enable
        self.navigationItem.rightBarButtonItem?.isEnabled = enable
    }
    
    private func getImage() {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.mediaTypes = [kUTTypeImage as String]
        imagePicker.allowsEditing = true
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        
        var imageSelected:UIImage?
        
        if let editImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            imageSelected = editImage
        }
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            imageSelected = pickedImage
        }
        
        
        if let image = imageSelected {
            
            if (isProfilePhoto) {
                
                imageViewPhoto.image = image
                self.view.isUserInteractionEnabled = false
                self.disableView(enable: false)
                self.showLoading()
                self.userBusiness.uploadPhoto(image, completion: { (url, error) in
                    if error == nil {
                        self.userData.photo = url!
                        self.hideLoading()
                        self.disableView(enable: true)
                    }else{
                        self.alertMensagem(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                        self.hideLoading()
                        self.disableView(enable: true)
                    }
                })
            }
            else {
                imageViewCover.image = image
                self.showLoading()
                self.disableView(enable: false)
                self.userBusiness.uploadCover(image, completion: { (url, error) in
                    if error == nil {
                        self.userData.cover = url!
                        self.hideLoading()
                        self.disableView(enable: true)
                    }else{
                        self.alertMensagem(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                        self.hideLoading()
                        self.disableView(enable: true)
                    }
                })
            }
        }
        
        
        dismiss(animated: true, completion: nil)
        
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
