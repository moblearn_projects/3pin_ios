//
//  PeopleTravelViewController.swift
//  threepin
//
//  Created by Anderson Silva on 18/06/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit

class PeopleTravelViewController: BaseViewController {

    @IBOutlet weak var tbView: UITableView!
    
    var idTravel:String!
    var travelBusiness: TravelBusiness!
    var objUser:[UserModel]!
    var objUsuario = [UserModel]()
    var total : Int = 0
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.addBackButton()
        getPeople()
        self.tbView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        travelBusiness = TravelBusiness.init()
        self.userBusiness = UserBusiness.init()
        self.tbView.delegate = self
        
        let peopleNib = UINib(nibName: "PeopleTravelTableViewCell", bundle: nil)
        self.tbView.register(peopleNib, forCellReuseIdentifier: "cellPeopleTravel")
        self.tbView.tableFooterView = UIView()
        self.view.addSubview(self.tbView)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        travelBusiness.removeObserve()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getPeople() {
        
        self.travelBusiness.getPeopleTravels(travelID: idTravel, uid: self.userBusiness.repository.getUid()) { (user, error) in
            
            guard user != nil else {
                self.alertMensagem(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                return
            }
            
            self.objUser = user
            self.total = self.objUser.count
            self.tbView.reloadData()
        }
        
    }
    
    func removeUser(indexPath:IndexPath) {
        
        self.travelBusiness.removeFriendTravel(travelID: self.idTravel, uidFriend: self.objUser[indexPath.row].uid!) { (success, error) in
            if success! {
                self.total = self.total - 1
                self.objUser.remove(at: indexPath.row)
                self.tbView.reloadData()
            }
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PeopleTravelViewController : UITableViewDelegate, UITableViewDataSource, PeopleTravelTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return total
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tbView.dequeueReusableCell(withIdentifier: "cellPeopleTravel", for: indexPath) as! PeopleTravelTableViewCell
        cell.delegate = self
        cell.setup(user: self.objUser[indexPath.row])
        return cell
    }
    
    func btnRemove(in cell: PeopleTravelTableViewCell) {
        if let indexPath = self.tbView.indexPath(for: cell) {
            self.removeUser(indexPath: indexPath)
        }
    }
    
}
