//
//  ProfileFriendViewController.swift
//  threepin
//
//  Created by Anderson Silva on 29/05/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Kingfisher

class ProfileFriendViewController: BaseViewController {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnTravels: UIButton!
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnImg: UIButton!
    @IBOutlet weak var btnDesfazer: UIButton!
    @IBOutlet weak var btnTrip: UIButton!
    
    var uid:String!
    var friends = [Friends]()
    var addUser:Bool = false
    var objFriend:Friends?
    var objUser:UserModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()                
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.addBackModal()
        self.loadPeople(id: self.uid)
        self.getFriendsPendent(userID: (self.userBusiness?.repository.getUid())!)
        self.ref?.observe(.value, with: { (snapshot) in
            if let value = snapshot.value {
                self.user = UserModel.init(value:value as! [String : NSObject])
                self.inputData()
            }
        })
        
        getUser()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.userBusiness.removeObserveUser()
    }
    
    func loadPeople(id:String){
        self.btnDesfazer.addShaddows()
        self.btnTrip.addShaddows()
        self.userBusiness = UserBusiness.init()
        
        self.ref = Database.database().reference().child("users").child(id);
        
        self.userBusiness.getCurrentUser { (user, sucess, error) in
            guard user != nil else {
                return
            }

            self.getFriends()
            self.objUser = user
        }
        
        imgProfile.asCircle()
        self.collectionView.delegate = self
        
    }
    
    func getUser() {
        
        self.userBusiness.getUserWithUid(self.uid) { (user, sucess, error) in
            guard user != nil else {
                self.alertMensagem(title: "Ops!", msg: error!, btn: "OK")
                return
            }
            
            self.objFriend = Friends.init(value: ["accept":false as AnyObject,
                                                  "acceptMe":true as AnyObject,
                                                  "nome":user?.name as AnyObject,
                                                  "photo":user?.photo as AnyObject,
                                                  "uid":user?.uid as AnyObject])
        }
    }
    
    func checkAcceptFriend() {
        
        self.userBusiness.acceptFriend(self.uid) { (success, error) in
            guard error == nil else {
                self.alertMensagem(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                return
            }
            
            if !success {
                self.btnDesfazer.isHidden = true
                self.btnImg.isHidden = true
            }else{
                self.userAdd()
            }
        }
        
    }
    
    func addFriend() {
        self.showLoading()
        self.userBusiness.addFriend(self.objFriend!) { (success, error) in
            guard error == nil else {
                self.hideLoading()
                self.alertMensagem(title: "Ops!", msg: error as! String, btn: "OK")
                return
            }
            if success! {
                self.hideLoading()
                self.userBusiness.addUserInFriend(self.objFriend!, objUser: self.objUser!, completion: { (success, error) in
                    self.showLoading()
                    guard error == nil else {
                        self.hideLoading()
                        self.alertMensagem(title: "Ops!", msg: error as! String, btn: "OK")
                        return
                    }
                    self.hideLoading()
                    self.backButtonModal()
                })
            }
        }
    }
    
    func userAdd() {
        self.btnImg.setImage(UIImage(named: "add-amigo"), for: .normal)
        self.btnDesfazer.setTitle("Adicionar amigo", for: .normal)
        self.addUser = true
    }
    
    func desfazerAmizade(){
        self.btnImg.setImage(UIImage(named: "remove-friend"), for: .normal)
        self.btnDesfazer.setTitle("Desfazer amizade", for: .normal)
    }
    
    func esconderBtnAmizade(){
        self.btnDesfazer.isHidden = true
        self.btnImg.isHidden = true
    }
    
    func getFriends() {
        self.userBusiness.getFriends(self.uid) { (friend, error) in
            guard error == nil else {
                self.alertMensagem(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                return
            }
            
            if self.addUser {

                if friend!.count > 0 {
                
                    let idUser = self.userBusiness?.repository.getUid()
                    
                    for f in friend! {
                        
                        if f.accept! && f.acceptMe! {
                            self.friends.append(f)
                        }
                        
                        if f.uid == idUser {
                            
                            if !f.accept! {
                                self.esconderBtnAmizade()
                                break
                            }
                            
                            self.desfazerAmizade()
                            self.addUser = false
                            break;
                        }else{
                            self.userAdd()
                        }
                    }
                }else{
                    self.userAdd()
                }
                self.collectionView.reloadData()
            }else{
                self.userBusiness.getFriendsCollection(self.uid, completion: { (friend, error) in
                    guard error == nil else {
                        self.alertMensagem(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                        return
                    }
                    self.friends = friend!
                    self.collectionView.reloadData()
                })
            }
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func inputData() {
        
        if let url = user?.cover {
            imgCover.kf.setImage(with: URL(string:url)!)
        }
        
        if let url = user?.photo {
            imgProfile.kf.setImage(with: URL(string:url)!)
        }
        
        lblName.text = user?.name
        lblAddress.text = user?.address
        lblTotal.text = String(format: "Visitou %d lugares", (user?.travels)!)
    }
    
    func deleteFriend() {
        self.showLoading()
        self.userBusiness.deleteFriend(self.uid) { (sucess, error) in
            if sucess! {
                self.hideLoading()
                self.backButtonModal()
            }else{
                self.alertMensagem(title: "Ops!", msg: (error?.localizedDescription)!, btn: "OK")
                self.hideLoading()
            }
        }
    }
    
    @IBAction func addTrip(_ sender: Any) {
        self.performSegue(withIdentifier: "addTrip", sender: self.uid)
    }

    @IBAction func btnUnfollow(_ sender: Any) {
        if self.addUser {
            self.addFriend()
        }else{
            self.deleteFriend()
        }
    }
    
    @IBAction func btnViagem(_ sender: Any) {
        self.performSegue(withIdentifier: "segueViagens", sender: nil)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "addTrip" {
            
            let vc = segue.destination as! AddFriendTravelViewController
            vc.uidFriend = sender as! String
            
        }
        
        if segue.identifier == "segueViagens" {
            
            let story = UIStoryboard(name: "Travel", bundle: nil)
            let vc    = story.instantiateViewController(withIdentifier: "ItemTravel") as! TravelsViewController
            vc.home = false
            vc.uid  = self.user.uid
            let nav   = UINavigationController(rootViewController: vc)
            nav.navigationBar.isTranslucent = true
            nav.navigationBar.barTintColor = UIColor.colorNavibar()
            nav.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
            self.present(nav, animated: true, completion: nil)
            
        }
    }
 

}
extension ProfileFriendViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard self.friends.count > 0 else {
            return 0
        }
        return self.friends.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellFriends", for: indexPath as IndexPath) as! PhotoCollectionViewCell
        cell.inputUser(self.friends[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.uid = self.friends[indexPath.row].uid!
        self.viewWillAppear(false)
    }
}
