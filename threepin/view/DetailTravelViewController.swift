//
//  DetailTravelViewController.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 03/02/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit
import Kingfisher

class DetailTravelViewController: BaseViewController {
    
    
    @IBOutlet weak var imgCover: UIImageView!

    @IBOutlet weak var viewPhotos: UIView!
    @IBOutlet weak var viewPeople: UIView!
    @IBOutlet weak var viewPlaces: UIView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblLeftDay: UILabel!
    @IBOutlet weak var lblNameAndDate: UILabel!
    @IBOutlet weak var btnEdit: UIBarButtonItem!
    
    var travel:Travel!
    var idTravel:String?
    var travelBusiness:TravelBusiness!
    var userData:UserModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        travelBusiness = TravelBusiness.init()
        self.userBusiness = UserBusiness.init()
        if travel?.uid != self.userBusiness.repository.getUid() {
            self.navigationItem.rightBarButtonItem = nil
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.seguePeople(_:)))
        viewPeople.addGestureRecognizer(tap)
        
        let tapPlace = UITapGestureRecognizer(target: self, action: #selector(self.seguePlaces(_:)))
        viewPlaces.addGestureRecognizer(tapPlace)
        
        let tapImage = UITapGestureRecognizer(target: self, action: #selector(self.segueImages(_:)))
        viewPhotos.addGestureRecognizer(tapImage)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        travelBusiness.getTravelWithID(travel.id!) { (travel, error) in
            guard error == nil else {
                self.back(nil)
                return
            }
            
            self.travel = travel
            self.updateData()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        travelBusiness.removeObserve()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func seguePeople(_ sender : UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "tapPeople", sender: self.travel.id!)
    }
    
    func seguePlaces(_ sender : UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "sgPlacesTravel", sender: self.travel.id!)
    }
    
    func segueImages(_ sender : UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "open_images", sender: self.travel.id!)
    }
    
    private func updateData() {
        
        if let url = travel.cover {
            self.imgCover.kf.setImage(with: URL(string:url)!)
        }
        else {
            self.imgCover.image = UIImage(named: "ic_img_empty_travel")
        }
        
        self.title = travel.name
        
        
        let startDate = Date.init(milliseconds: travel.startDate!)
        let endDate = Date.init(milliseconds: travel.endDate!)
        
        let days = Date().daysBetween(date: startDate);
        
        if days > 0 {
            lblLeftDay.text = String(format: "%d dias para o início", days)
        }
        
        
        lblNameAndDate.text = String(format: "%@ - %@", travel.destiny!, startDate.dateString(format: "MM/yyyy"))
        
        
        lblDate.text = String(format: "De %@ até %@",
                              startDate.dateString(format:"dd/MM/YYYY"),
                              endDate.dateString(format:"dd/MM/YYYY"))
        
    }
    
    @IBAction func back(_ sender: UIBarButtonItem?) {
        _ = navigationController?.popViewController(animated: true)
    }

    @IBAction func btnEdit(_ sender: Any) {
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if ("edit_travel" == segue.identifier) {
            let viewController = segue.destination as! NewTravelViewController
            viewController.travel = self.travel
        }else if ("open_images" == segue.identifier) {
            let viewController = segue.destination as! ImagesCollectionViewController
            viewController.travel = self.travel
        }else if (segue.identifier == "tapPeople"){
            
            let vc = segue.destination as! PeopleTravelViewController
            vc.idTravel = sender as! String
            
        }else if(segue.identifier == "sgPlacesTravel") {
            let vc = segue.destination as! PlacesTravelViewController
            vc.idTravel = sender as! String
            vc.titulo = self.travel.name!
        }
    }

}
