//
//  Extensions.swift
//  threepin
//
//  Created by Anderson Silva on 22/05/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    public func dch_checkDeallocation(afterDelay delay: TimeInterval = 2.0) {
        let rootParentViewController = dch_rootParentViewController
        
        // We don’t check `isBeingDismissed` simply on this view controller because it’s common
        // to wrap a view controller in another view controller (e.g. in UINavigationController)
        // and present the wrapping view controller instead.
        if isMovingFromParentViewController || rootParentViewController.isBeingDismissed {
            let type = type(of: self)
            let disappearanceSource: String = isMovingFromParentViewController ? "removed from its parent" : "dismissed"
            
            DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: { [weak self] in
                assert(self == nil, "\(type) not deallocated after being \(disappearanceSource)")
            })
        }
    }
    
    private var dch_rootParentViewController: UIViewController {
        var root = self
        
        while let parent = root.parent {
            root = parent
        }
        
        return root
    }
    
}

extension UIColor {
    
    static func colorGreen() -> UIColor {
        return UIColor(red: 39/255.0, green: 134/255.0, blue: 93/255.0, alpha: 1.0)
    }
    
    static func colorNavibar() -> UIColor {
        return UIColor(red: 254/255.0, green: 127/255.0, blue: 127/255.0, alpha: 1.0)
    }
    
}

extension UIButton {
    
    func addBlurEffect(style:UIBlurEffectStyle) {
        
        let effect = UIBlurEffect(style: style)
        let blur = UIVisualEffectView(effect: effect)
        blur.isUserInteractionEnabled = false
        self.insertSubview(blur, at: 0)
        
    }
    
    func addShaddows() {
        
        self.layer.shadowColor  = UIColor.black.cgColor //UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        self.layer.shadowOffset = CGSize.zero //CGSize(width: 0.0, height: 2.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 10.0
        self.layer.masksToBounds = false
        self.layer.cornerRadius = 4.0
        
    }
}

extension UITextField {
    func addBlur(style:UIBlurEffectStyle, view:UIView) {
        if !UIAccessibilityIsReduceTransparencyEnabled() {
            let effect = UIBlurEffect(style: style)
            let blurEffectView = UIVisualEffectView(effect: effect)
            blurEffectView.frame = self.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.backgroundColor = UIColor.clear
            
            self.background = view.imageWithView()
        }
    }
}

extension Date {
    
    var ontem : String {
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "dd-MM-yyyy"
        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: self)
        return dateFormatter1.string(from: yesterday!)

    }
    
    var hoje : String {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "dd-MM-yyyy"
        return dateFormatter1.string(from: self)
    }
    
    var amanha : String {
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "dd-MM-yyyy"
        return dateFormatter1.string(from: Calendar.current.date(byAdding: .day, value: +1, to: self)!)
        
    }
    
}

extension UIView {
    func imageWithView() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, 0.0)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
