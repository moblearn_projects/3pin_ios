//
//  UIImage.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 09/02/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import Foundation
import UIKit

public extension UIImage {
    
    static func croppedImage(image:UIImage, cropRect: CGRect) -> UIImage{
        
        UIGraphicsBeginImageContext(cropRect.size)
        image.draw(at: CGPoint.init(x: -cropRect.origin.x, y: -cropRect.origin.y))
        let cropped = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return cropped!
    }
}
