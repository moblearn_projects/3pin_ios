//
//  AppSession.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 04/02/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class AppSession {
    
    static let sharedInstance : AppSession = {
        let instance = AppSession()
        return instance
    }()
    
    var ref : DatabaseReference? = nil;
    var currentUser: UserModel? = nil
    var likes: Dictionary<String, AnyObject>? = nil
    var uid:String = (Auth.auth().currentUser?.uid)!
    var myFriends : [Friends]?
    var friendsToFriends = [Friends]()
    
    
    func addCurrentUser(_ user:UserModel) {
        self.currentUser = user
        self.ref = Database.database().reference().child("users").child((currentUser?.uid)!);
        
        observeUser();
        getLikes()
    }
    
    private func observeUser() {
        self.ref?.observe(.value, with: { (snapshot) in
            if let value = snapshot.value {
                self.currentUser = UserModel.init(value:value as! [String : NSObject])
            }
        })
    }
    
    private func getLikes() {
        _ = ref?.child("likes").observe(DataEventType.value, with: { (snapshot) in
            self.likes = snapshot.value as? [String : AnyObject]
        })
    }
    
    func isLikeTravel(_ travelID:String) -> Bool {
        guard likes != nil else {
            return false
        }
        let travels = likes?["travels"] as! [String : AnyObject]
        return travels.keys.contains(travelID)
    }
    
    func isLikeImages(_ travelID:String) -> Bool {
        guard likes != nil else {
            return false
        }
        let travels = likes?["images"] as! [String : AnyObject]
        return travels.keys.contains(travelID)
    }
    
    func isLikePlaces(_ travelID:String) -> Bool {
        guard likes != nil else {
            return false
        }
        let travels = likes?["places"] as! [String : AnyObject]
        return travels.keys.contains(travelID)
    }
}
