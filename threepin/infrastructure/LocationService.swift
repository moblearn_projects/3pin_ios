//
//  LocationService.swift
//  threepin
//
//  Created by Anderson Silva on 05/11/2017.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationServiceDelegate : class {
    func tracingLocation(currentLocation:CLLocation)
    func tracingLocationFailWithError(error:NSError)
}

class LocationService : NSObject, CLLocationManagerDelegate {
    
    static let sharedInstance : LocationService = {
        let instance = LocationService()
        return instance
    }()
    
    var locationManager : CLLocationManager!
    var lastLocation: CLLocation?
    weak var delegate : LocationServiceDelegate?
    
    override init() {
        super.init()
        determineMyLocation()
    }
    
    func determineMyLocation() {
        
        locationManager = CLLocationManager()
        
        if CLLocationManager.authorizationStatus() == .notDetermined || CLLocationManager.authorizationStatus() == .denied {
            locationManager.requestAlwaysAuthorization()
        }
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 200
        locationManager.delegate = self
    }
    
    func startUpdatingLocation() {
        print("Starting Location Updates")
        self.locationManager?.startUpdatingLocation()
    }
    
    func stopLocation() {
        print("Stop Location Updates")
        locationManager.stopUpdatingLocation()
    }
    
    private func updateLocation(currentLocation:CLLocation) {
        
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.tracingLocation(currentLocation: currentLocation)
        
    }
    
    private func updateLocationDidFailWithError(error:NSError){
        
        guard let delegate = self.delegate else {
            return
        }
        
        delegate.tracingLocationFailWithError(error: error)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        let userLocation:CLLocation = locations[0] as CLLocation
//
//        self.latitude = userLocation.coordinate.latitude
//        self.longitude = userLocation.coordinate.longitude
        
        guard let location = locations.last else {
            return
        }
        
        self.lastLocation = location
        updateLocation(currentLocation: location)
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error: \(error.localizedDescription)")
        updateLocationDidFailWithError(error: error as NSError)
    }
    
}
