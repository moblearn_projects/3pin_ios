//
//  Permissoes.swift
//  threepin
//
//  Created by Anderson Silva on 12/11/2017.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import Foundation

enum Permissoes : String {
    
    case PRIVADO = "PRIVADO"
    case AMIGOS  = "AMIGOS"
    case AMIGOS_AMIGOS = "AMIGOS DOS AMIGOS"
    case PUBLICA = "PÚBLICA"
    
    static let allValues = [PRIVADO.rawValue, AMIGOS.rawValue, AMIGOS_AMIGOS.rawValue, PUBLICA.rawValue]
    
}
