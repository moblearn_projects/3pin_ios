//
//  UIImageView.swift
//  threepin
//
//  Created by Felipe Arimateia Terra Souza on 13/02/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import Foundation
import UIKit


extension UIImageView{
    
    func asCircle(){
        self.layer.cornerRadius = self.frame.width / 2;
        self.layer.masksToBounds = true
    }
    
}
