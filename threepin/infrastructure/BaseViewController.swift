//
//  BaseViewController.swift
//  threepin
//
//  Created by Anderson Silva on 22/05/17.
//  Copyright © 2017 Moblearn. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import Kingfisher
import GooglePlaces

protocol BaseViewControllerDelegate : class {
    func alertMsg(title:String,msg:String,btn:String)
    func loadingShow()
    func loadingHide()
    func viewDisable(enable:Bool)
}

class BaseViewController: UIViewController {

    var loadingView : UIViewController?
    var userBusiness: UserBusiness!
    var ref : DatabaseReference? = nil;
    var user:UserModel!
    var allFriendsPendents = [Friends]()
    var placesClient : GMSPlacesClient!
    
    let searchBar = UISearchBar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let recognizer:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(BaseViewController.backButton))
        recognizer.direction = .right
        self.view.addGestureRecognizer(recognizer)
    }        

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchNavigation(searchBar:UISearchBar, placeholder:String, barTintColor:UIColor, view:UIViewController, showCancelButton:Bool=true, textField:Bool=true){
        searchBar.sizeToFit()
        searchBar.barStyle = .default
        searchBar.barTintColor = barTintColor
        searchBar.placeholder = placeholder
        searchBar.tintColor = UIColor.white
        searchBar.setShowsCancelButton(showCancelButton, animated: true)
        
        if textField {
            searchBar.isSearchBarTextField()
        }else{
            searchBar.endEditing(true)
        }
        
        view.navigationItem.titleView = searchBar
    }
    
    func downGestureModal(){
        let recognizer:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(BaseViewController.backButtonModal))
        recognizer.direction = .down
        self.view.addGestureRecognizer(recognizer)
    }
    
    func getFriendsPendent(userID:String){
        
        self.userBusiness.getFriendsPendent(userID) { (friend, error) in
            guard friend != nil else {
                return
            }
            
            self.allFriendsPendents = friend!
            if self.allFriendsPendents.count > 0 {
                self.tabBarController?.tabBar.items?[1].badgeValue = "\(self.allFriendsPendents.count)"
            }else{
                self.tabBarController?.tabBar.items?[1].badgeValue = nil
            }
        }
    }
    
    func getIdLogado() -> String {
        var uid = ""
        self.userBusiness.getCurrentUser { (user, success, error) in
            guard user != nil else {
                return
            }
            uid = user!.uid!
        }
        return uid
    }
    
    func disableView2(enable:Bool) {
        self.view.isUserInteractionEnabled = enable
        self.navigationItem.leftBarButtonItem?.isEnabled = enable
        self.navigationItem.rightBarButtonItem?.isEnabled = enable
    }
    
    func logOff() {
        try! Auth.auth().signOut()
        self.dismiss(animated: true, completion: nil)
    }
    
    func addBackButton() {
        let barButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self , action: #selector(BaseViewController.backButton))
        barButton.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = barButton
    }
    
    func backButton(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func addBackModal() {
        let barButton = UIBarButtonItem(image: UIImage(named: "back"), style: UIBarButtonItemStyle.plain, target: self , action: #selector(BaseViewController.backButtonModal))
        barButton.tintColor = UIColor.white
        navigationItem.leftBarButtonItem = barButton
    }
    
    func backButtonModal() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func alertMensagem(title:String, msg:String, btn:String){
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: btn, style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showLoading() {
        DispatchQueue.main.async {
            self.loadingView = UIStoryboard(name: "Loading", bundle: nil).instantiateViewController(withIdentifier: "LoadingView")
            self.loadingView!.view.alpha = 0.0
            self.view.addSubview(self.loadingView!.view)
            
            UIView.animate(withDuration: 0.5, animations: {
                self.loadingView!.view.alpha = 0.8
            })
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.8, animations: {
                self.loadingView!.view.alpha = 0.0
            }, completion: { (finished) in
                if finished {
                    self.loadingView?.view.removeFromSuperview()
                    self.loadingView = nil
                }
            })
        }
    }
    
    func tapDismiss(_ sender: UITapGestureRecognizer) {        
        self.dismiss(animated: true, completion: nil)
    }
    
    func addBlurArea(area: UIView, style: UIBlurEffectStyle) {
        let blurEffect = UIBlurEffect(style: style)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = area.bounds
        self.view.insertSubview(blurEffectView, at: 0)
        
    }        

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension BaseViewController : BaseViewControllerDelegate {
    func alertMsg(title: String, msg: String, btn: String) {
        self.alertMensagem(title: title, msg: msg, btn: btn)
    }
    
    func loadingShow(){
        self.showLoading()
    }
    func loadingHide(){
        self.hideLoading()
    }
    func viewDisable(enable:Bool) {
        self.disableView2(enable: enable)
    }
}
