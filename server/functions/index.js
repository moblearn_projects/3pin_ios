var functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.countLikesTravels = functions.database.ref('travels_likes/{travelId}').onWrite(event => {
    const key = event.data.key;
    return admin.database().ref("travels").child(key)
        .child("count_likes").set(event.data.numChildren());
});

exports.searchPlaces = functions.https.onRequest((req, res) => {
    
    const query = req.query.q;
    

});